package com.luisignacio.com.near.Models.Database;

/**
 * Created by Nacho on 14/04/2018.
 */

public class Offers {

    private String id;
    private String title;
    private String description;
    private boolean enabled;
    private String photoUrl;
    private String[] likes;
    private String establishment_id;

    public Offers() {
    }

    public Offers(String id, String title, String description, boolean enabled, String photoUrl, String[] likes, String establishment_id) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.enabled = enabled;
        this.photoUrl = photoUrl;
        this.likes = likes;
        this.establishment_id = establishment_id;
    }

    public Offers(String title, String description, boolean enabled, String photoUrl) {
        this.title = title;
        this.description = description;
        this.enabled = enabled;
        this.photoUrl = photoUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String[] getLikes() {
        return likes;
    }

    public void setLikes(String[] likes) {
        this.likes = likes;
    }

    public String getEstablishment_id() {
        return establishment_id;
    }

    public void setEstablishment_id(String establishment_id) {
        this.establishment_id = establishment_id;
    }
}
