package com.luisignacio.com.near.BaseModified;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.widget.ImageView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.ListenerRegistration;
import com.luisignacio.com.near.Activitys.AccessActivity;
import com.luisignacio.com.near.Fragments.DetailEstablishmentFragment;
import com.luisignacio.com.near.Fragments.FirstFrameFragment;
import com.luisignacio.com.near.Fragments.MapsFragment;
import com.luisignacio.com.near.Fragments.NearFragment;
import com.luisignacio.com.near.Models.Database.Establishment;
import com.luisignacio.com.near.Models.Database.Table;
import com.luisignacio.com.near.Models.Database.User;
import com.luisignacio.com.near.R;
import com.luisignacio.com.near.interfaces.DownloadEstablishmentCallback;
import com.luisignacio.com.near.interfaces.OnTableChange;

import org.imperiumlabs.geofirestore.GeoFirestore;
import org.imperiumlabs.geofirestore.GeoQuery;
import org.imperiumlabs.geofirestore.GeoQueryDataEventListener;

import java.util.ArrayList;
import java.util.Map;

import static com.luisignacio.com.near.Utils.Constants.USERS;
import static com.luisignacio.com.near.Utils.Utils.getFirestoreInstanceWithSetting;

public abstract class BaseMain extends BaseActivity implements GeoQueryDataEventListener, FirebaseAuth.AuthStateListener, FirstFrameFragment.OnFirstFragmentInteractionListener, MapsFragment.OnFragmentMapsInteractionListener{

    protected static final double QUERY_RADIUS = 5;
    private static final String TAG = BaseMain.class.getSimpleName();
    private static final String NEAR_FRAGMENT = "near";
    private static final String FAVORITES_FRAGMENT = "favorites";

    protected ImageView mConexionState;

    protected FirebaseAuth mAuth;

    protected DocumentReference userFirestoreReference;
    protected DocumentReference tableFirestoreReference;
    protected ListenerRegistration firestoreUserListener;
    protected ListenerRegistration firestoreTableListener;
    protected FusedLocationProviderClient mFusedLocationProviderClient;
    protected GeoFirestore geoFirestore;
    protected GeoQuery geoQuery;

    protected OnTableChange mTableListener;
    protected DownloadEstablishmentCallback nearbyCallback;

    protected User user;
    protected Table mTable;
    protected Map<String, Fragment> mFragments;

    /**
     * Método necesario para herencia de Base Activity e inicialización de ButterKnife
     * @return
     */
    @Override
    public int getViewId() {
        return R.layout.activity_main;
    }

    /**
     * Get para obtener el usuario en los diferentes fragmentos
     * @return
     */
    public User getUser(){
        return user;
    }

    public Table getTable(){
        return mTable;
    }

    /**
     * Método con el que comprobamos si el usuario se ha desconectado
     * @param newState
     * @return
     */
    protected boolean isDisconnected( User newState ){
        return (user.getTable_id() != null || user.getEstablishment_id() != null) && (newState.getEstablishment_id() == null && newState.getTable_id() == null);
    }

    /**
     * Método con el que comprobamos si el usuario está conectado
     * @param newState
     * @return
     */
    protected boolean isConnected( User newState ){
        return (user.getTable_id() == null || user.getEstablishment_id() == null) && (newState.getEstablishment_id() != null && newState.getTable_id() != null);
    }

    /**
     * Agregamos el listener que tenemos en nuestro fragmento para poder escuchar cambios
     * @param listener Listener para interactuar entre la actividad y el fragmento
     */
    public void setOnTableChangeListener(OnTableChange listener){
        mTableListener = listener;
    }

    /**
     * Agregamos el callback para poder interactuar entre la actividad y el fragmento
     * @param callback Callback para mandarnos la lista de establecimientos cercanos utilizando el método
     *                 de la actividad y evitar la duplicidad de código generado.
     */
    public void setOnDownloadNearbyCallback(DownloadEstablishmentCallback callback){
        nearbyCallback = callback;
    }

    /**
     * Método con el que obtenemos la lista de favoritos, utilizamos en los fragmentos que nos haga falta
     * @return
     */
    public ArrayList<String> getFavorites(){
        return (user!=null) ? user.getFavorites() : new ArrayList<>();
    }

    /**
     * Método para inicializar la localización y realizar consulta en la base de datos
     */
    protected void getLastKnowLocation() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }else{

            mFusedLocationProviderClient.getLastLocation().addOnSuccessListener(location -> {
                if (location != null) {
                    final GeoPoint geoLocation = new GeoPoint(location.getLatitude(), location.getLongitude());
                    geoQuery = geoFirestore.queryAtLocation(geoLocation, QUERY_RADIUS);
                    geoQuery.removeAllListeners();
                    geoQuery.addGeoQueryDataEventListener(this);
                }else{
                    getLastKnowLocation();
                }
            });

        }

    }

    /**
     * Método con el que ponemos el iconoo de nuestro menú a conectado o desconectado, dependiendo del estado en el que se
     * encuentre nuestro usuario
     */
    protected void checkState(){

        if ( user != null && user.getEstablishment_id() != null && user.getTable_id() != null )
            mConexionState.getBackground().setLevel(0);
        else
            mConexionState.getBackground().setLevel(1);

    }

    /**
     * Método con el que cambiamos de fragmento a presentar
     * @param fragment
     * @param backStack --> Si queremos poner en la pila o directamente mostrar
     */
    protected void changeFragment(Fragment fragment, boolean backStack ){

        final FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        ft.setCustomAnimations(R.animator.fragment_slide_left_enter,
                R.animator.fragment_slide_left_exit,
                R.animator.fragment_slide_right_enter,
                R.animator.fragment_slide_right_exit);

        if ( !backStack ) {
            Log.i(TAG, "Introduzco fragmento -----> " + fragment.getClass().getSimpleName());
            if ( fragment instanceof FirstFrameFragment && fm.getBackStackEntryCount() != 0)
                clearStack(fm);
            ft.replace(R.id.content_main, fragment).commit();
        }else {
            ft.replace(R.id.content_main, fragment).addToBackStack("tag").commitAllowingStateLoss();
        }

    }

    private void clearStack(FragmentManager fm){
        Log.i(TAG, "Limpiando la pila de fragmentos!!!! total --> " + fm.getBackStackEntryCount());
        for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
    }

    /**
     * Método que utilizamos para guardar a el estado nuevo de un usuario, cuando este se conecta a una mesa
     * y un establecimiento
     */
    protected void saveNewState(){

        getFirestoreInstanceWithSetting().collection(USERS).document(user.getUuid()).set(user);
        checkState();

    }

    /**
     * Listener para escuchar los cambios de estado en una mesa en la que estamos sentados
     * @param establishment_uuid
     * @param uuid_table
     */
    protected void addTableListener(String establishment_uuid, String uuid_table) {

        tableFirestoreReference = getFirestoreInstanceWithSetting().collection("establishment").document(establishment_uuid).collection("tables").document(uuid_table);
        firestoreTableListener = tableFirestoreReference.addSnapshotListener((documentSnapshot, e) -> {
            if ( e != null ){
                Log.e(TAG, "Error al encontrar la referencia de la mesa en la base de datos --> " + e.getMessage());
                return;
            }

            if ( documentSnapshot != null && documentSnapshot.exists() ){
                mTable = documentSnapshot.toObject(Table.class);
                if ( mTableListener != null )
                    mTableListener.onTableChangeListener(mTable);
            }else{
                Log.e(TAG, "Error Firestore ... el documento es nulo o la referencia no existe!!");
            }
        });

    }

    /**
     * Método con el que eliminamos los listener a las referencias de usuario y mesa en nuestra base de datos
     */
    protected void deleteFirestoreListener(){
        if ( userFirestoreReference != null )
            firestoreUserListener.remove();
        if ( tableFirestoreReference != null )
            firestoreTableListener.remove();
    }

    /**
     * Método que utilizamos para cerrar la sesión de usuario cuando este lo desee
     */
    protected void closeSession() {
        if ( mAuth != null )
            mAuth.signOut();
    }

    /**
     * Método que se llama cuando cambia de estado nuestro usuario de firebase
     * @param firebaseAuth
     */
    @Override
    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

        Log.i(TAG, "cambio estado usuario --> " + firebaseAuth.toString());
        if ( firebaseAuth.getCurrentUser() != null)
            Log.i(TAG, "cambio estado usuario --> " + firebaseAuth.getCurrentUser().getUid());

        if ( firebaseAuth.getCurrentUser() == null ){

            Intent intent = new Intent(this, AccessActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        }

    }

    /**
     * Método que se llama cuando FirstFragment realiza una acción
     * @param viewId
     */
    @Override
    public void onFirstFragmentInteraction(int viewId, ArrayList<Establishment> establishments) {

        Fragment fragment;

        switch (viewId){
            case R.id.near:
                fragment = NearFragment.newInstance(establishments, "Cerca de mí", user.getFavorites(), false);
                mFragments.put(NEAR_FRAGMENT, fragment);
                changeFragment(fragment, true);
                Log.i(TAG, "Near pressed");
                break;
            case R.id.maps:
                changeFragment(MapsFragment.newInstance(establishments), true);
                Log.i(TAG, "Maps pressed");
                break;
            case R.id.favorite:
                fragment = NearFragment.newInstance(establishments, "Favoritos", user.getFavorites(), true);
                mFragments.put(FAVORITES_FRAGMENT, fragment);
                changeFragment(fragment, true);
                Log.i(TAG, "Favorite pressed");
                break;
        }

    }

    /**
     * listener para cuando seleccionamos un establecimiento
     * @param establishment
     */
    @Override
    public void onEstablishmentSelected(Establishment establishment) {
        changeFragment(DetailEstablishmentFragment.newInstance(establishment, user.getFavorites()), true);
    }


}
