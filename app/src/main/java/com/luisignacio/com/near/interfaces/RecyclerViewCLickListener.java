package com.luisignacio.com.near.interfaces;

import android.view.View;

/**
 * Created by Nacho on 19/05/2018.
 */

public interface RecyclerViewCLickListener {
    void onClick(View view, int position);
}
