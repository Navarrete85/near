package com.luisignacio.com.near.Activitys;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.luisignacio.com.near.R;
import com.luisignacio.com.near.Utils.Utils;


/**
 * Actividad encargada de tomar la decisión pertinente cuando la aplicación es lanzada, o nos manda a Access o a Main,
 * tambien se encarga de pedir los permisos de localización que vamos a necesitar para poder utilizar la app
 */
public class SplashActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private boolean acept;

    private static final int REQUEST_PERMISSION = 123;
    private static final String TAG = "Splash";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mAuth = FirebaseAuth.getInstance();
        //Si necesitamos activar el depurador de error de firebase database, lo necesitamos en su día para ver si el token se refrescaba o de lo contrario caducaba y por eso nos arrojaba un error
        //FirebaseDatabase.getInstance().setLogLevel(Logger.Level.DEBUG);

        ActivityCompat.requestPermissions(SplashActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,
                android.Manifest.permission.BLUETOOTH, android.Manifest.permission.BLUETOOTH_ADMIN}, REQUEST_PERMISSION);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode){

            case REQUEST_PERMISSION:
                checkPermission(grantResults);
                break;

        }

    }

    private void checkPermission(int[] grantResults) {

        acept = true;

        if ( grantResults.length > 0 ){

            for( Integer item : grantResults ){

                if ( item == PackageManager.PERMISSION_DENIED ){

                    acept = false;

                    Utils.getAlertBuilder(R.string.denied_permission_title, R.string.denied_permission_msg, R.drawable.warning, this)
                            .setPositiveButton(R.string.ok, (dialogInterface, i) -> ActivityCompat.requestPermissions(SplashActivity.this,
                                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,
                                    android.Manifest.permission.BLUETOOTH, android.Manifest.permission.BLUETOOTH_ADMIN}, REQUEST_PERMISSION))
                            .setNegativeButton(R.string.no, (dialogInterface, i) -> System.exit(0)).show();

                }

            }

            if ( acept ){

                FirebaseUser user = mAuth.getCurrentUser();
                if ( user != null ) {
                    Log.i(TAG, ((user.getUid() != null) ? mAuth.getCurrentUser().getEmail() : "null") + " /primera linea");
                    Log.i(TAG, mAuth.getCurrentUser().toString());
                }
                //La validación del correo electrónico se elimina de esta condición
                if ( mAuth.getCurrentUser() != null /*&& mAuth.getCurrentUser().isEmailVerified()*/ ){
                    goToMain();
                }else{
                    goToAccess();
                }

            }

        }

    }

    private void goToAccess() {

        Intent intent = new Intent(this, AccessActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

    }

    private void goToMain() {

        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

    }
}
