package com.luisignacio.com.near.Utils;

/**
 * Created by Nacho on 14/04/2018.
 */

public final class Constants {
    //og.i(TAG, "FrameType = " +advData.getString("FrameType")+" AdvTxPower = "+advData.getString("AdvTxPower")+" Namespace = "+advData.getString("Namespace")+" Instance = "+advData.getString("Instance"));
    //Log.i(TAG, "FrameType = " +advData.getString("FrameType")+" Version = "+advData.getString("Version")+" Vbatt = "+advData.getString("Vbatt")+" Temp = "+advData.getString("Temp")+" AdvCount = "+advData.getString("AdvCount")+" TimeUp = "+advData.getString("TimeUp"));
    //Log.i(TAG, "FrameType = " +advData.getString("FrameType")+" Version = "+advData.getString("Version")+" EncryptedTLMData = "+advData.getString("EncryptedTLMData")+" Salt = "+advData.getString("Salt")+" IntegrityCheck = "+advData.getString("IntegrityCheck"));
    public static final int REQUEST_GOOGLE_SING_IN = 1006;
    public static final String USERS = "users";
    public static final String ESTABLISHMENTS = "establishment";
    public static final String FRAME_TYPE = "FrameType";
    public static final String ADVTXPOWER = "AdvTxPower";
    public static final String ESTABLISHMENT = "Namespace";
    public static final String TABLE = "Instance";
    public static final String TABLES = "tables";
    public static final String VERSION = "Version";
    public static final String VBATT = "Vbatt";
    public static final String TEMP= "Temp";
    public static final String ADVCOUNT= "AdvCount";
    public static final String ENCRYPTED_TLM_DATA= "EncryptedTLMData";
    public static final String SALT = "Salt";
    public static final String TIME_UP = "TimeUp";
    public static final String INTEGRITY_CHECK = "IntegrityCheck";
    public static final int MAP = 1, NEARBY = 2, FAVOTIES = 3;
    public static final int STATE_ORDER = 1, STATE_ORDER_BILL = 3, STATE_ATTENTION = 2, STATE_NORMAL = 0;


    public static final String NAME = "name",
                               EMAIL = "email",
                               PHOTO = "photo",
                               ESTABLISHMENT_ID = "establishment",
                               TABLE_ID = "table";


    public static final int ERROR_TABLE_NOT_FOUND = 0, ERROR_OCCUPIED_TABLE = 1;

}
