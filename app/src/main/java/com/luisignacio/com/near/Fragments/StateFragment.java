package com.luisignacio.com.near.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import static com.luisignacio.com.near.Utils.Constants.*;
import static com.luisignacio.com.near.Utils.Utils.getFirestoreInstanceWithSetting;

import com.luisignacio.com.near.BaseModified.BaseFragment;
import com.luisignacio.com.near.Models.Database.Table;
import com.luisignacio.com.near.R;
import com.luisignacio.com.near.Utils.Utils;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Nacho on 05/05/2018.
 */
public class StateFragment extends BaseFragment {

    private static final String TAG = StateFragment.class.getSimpleName();
    private static final String PARAM1 = "param1";
    private static final String PARAM2 = "param2";
    private static final String PARAM3 = "param3";

    @BindView(R.id.card_order) View mOrder;
    @BindView(R.id.card_attention) View mAttention;
    @BindView(R.id.card_order_bill) View mOrderBill;
    @BindView(R.id.wait_for) View mWait_For;

    private Table mTable;
    private String mEstablishment_id;
    private String mUuidTable;

    /**
     * Constructor por defecto
     */
    public StateFragment() {
    }

    public static StateFragment newInstance( Table table, String establishment_id, String uuidTable ) {
        Bundle args = new Bundle();
        args.putParcelable(PARAM1, table);
        args.putString(PARAM2, establishment_id);
        args.putString(PARAM3, uuidTable);
        StateFragment fragment = new StateFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mTable = (Table) getArguments().getParcelable(PARAM1);
            mEstablishment_id = getArguments().getString(PARAM2);
            mUuidTable = getArguments().getString(PARAM3);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        refreh();

        return mView;
    }

    @Override
    public int getFragmentReference() {
        return R.layout.state_fragment;
    }

    public void setTable(Table newState){
        mTable = newState;
        refreh();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreh();
    }

    private void refreh() {
        if ( mTable != null && mWait_For != null ){
            switch (mTable.getState()){
                case 0:
                    mWait_For.setVisibility(View.GONE);
                    enabledButton(true);
                    break;
                default:
                    enabledButton(false);
                    mWait_For.setVisibility(View.VISIBLE);
            }
        }
    }

    private void enabledButton(boolean enabled) {
        mOrder.setEnabled(enabled);
        mOrderBill.setEnabled(enabled);
        mOrderBill.setEnabled(enabled);
    }

    @OnClick({R.id.card_order, R.id.card_order_bill, R.id.card_attention})
    public void clickAction(View view) {
        switch (view.getId()){
            case R.id.card_order:
                changeState(1);
                break;
            case R.id.card_order_bill:
                changeState(3);
                break;
            case R.id.card_attention:
                changeState(2);
                break;
        }
    }

    private void changeState(int newState) {
        if (mTable == null)
            throw new NullPointerException("Excepción la mesa apunta a null...linea 129");

        mTable.setState(newState);
        mTable.setTime_of_state_change(Utils.getStringCurrentTime());
        refreh();
        getFirestoreInstanceWithSetting().collection(ESTABLISHMENTS).document(mEstablishment_id).collection(TABLES).document(mUuidTable).set(mTable);
    }

}
