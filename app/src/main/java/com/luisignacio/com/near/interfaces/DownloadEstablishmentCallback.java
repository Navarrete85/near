package com.luisignacio.com.near.interfaces;

import com.luisignacio.com.near.Models.Database.Establishment;

import java.util.ArrayList;

public interface DownloadEstablishmentCallback {

    void onDownloadFinished(int view, ArrayList<Establishment> establishment);

}
