package com.luisignacio.com.near.Models.Database;

/**
 * Created by Nacho on 14/04/2018.
 */

public class Tables {

    private String establishment_id;
    private Table[] tables;

    public Tables() {
    }

    public Tables(String establishment_id, Table[] tables) {
        this.establishment_id = establishment_id;
        this.tables = tables;
    }

    public String getEstablishment_id() {
        return establishment_id;
    }

    public void setEstablishment_id(String establishment_id) {
        this.establishment_id = establishment_id;
    }

    public Table[] getTables() {
        return tables;
    }

    public void setTables(Table[] tables) {
        this.tables = tables;
    }

}
