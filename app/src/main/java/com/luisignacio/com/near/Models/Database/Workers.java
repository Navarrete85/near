package com.luisignacio.com.near.Models.Database;

/**
 * Created by Nacho on 14/04/2018.
 */

public class Workers {

    private String  uuid;
    private String  name;
    private String  lastName;
    private String  photoUrl;
    private String  establishment;
    private boolean enabled;

    public Workers() {
    }

    public Workers(String uuid, String name, String lastName, String establishment, boolean enabled) {
        this.uuid = uuid;
        this.name = name;
        this.lastName = lastName;
        this.establishment = establishment;
        this.enabled = enabled;
    }

    public Workers(String uuid, String name, String lastName, String photoUrl, String establishment, boolean enabled) {
        this.uuid = uuid;
        this.name = name;
        this.lastName = lastName;
        this.photoUrl = photoUrl;
        this.establishment = establishment;
        this.enabled = enabled;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getEstablishment() {
        return establishment;
    }

    public void setEstablishment(String establishment) {
        this.establishment = establishment;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
