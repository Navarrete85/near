package com.luisignacio.com.near.interfaces;

/**
 * Created by Nacho on 26/05/2018.
 */

public interface ToolbarElevation {
    void setToolbarElevation(float elevation);
}
