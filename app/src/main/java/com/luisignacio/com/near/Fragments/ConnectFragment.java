package com.luisignacio.com.near.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.luisignacio.com.near.Activitys.MainActivity;
import com.luisignacio.com.near.Adapter.ViewPagerAdapter;
import com.luisignacio.com.near.BaseModified.BaseFragment;
import com.luisignacio.com.near.Models.Database.Establishment;
import com.luisignacio.com.near.Models.Database.Table;
import com.luisignacio.com.near.R;
import com.luisignacio.com.near.interfaces.OnTableChange;
import com.luisignacio.com.near.interfaces.ToolbarElevation;
import com.luisignacio.com.near.interfaces.ToolbarSetTitle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;

public class ConnectFragment extends BaseFragment implements OnTableChange{

    private static final String TAG = "ConnectFragment";

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private static final String FRAGMENT_STATE = "Estado",
                                FRAGMENT_OFFERT = "Ofertas",
                                FRAGMENT_DETAIL = "Detalle";
    private String[] key = {FRAGMENT_STATE, FRAGMENT_OFFERT, FRAGMENT_DETAIL};

    @BindView(R.id.tab_layout_id) TabLayout mTabLayout;
    @BindView(R.id.view_pager_id) ViewPager mViewPager;

    private ViewPagerAdapter mPagerAdapter;
    private Establishment mEstablishment;
    private ArrayList<String> mFavorites;
    private Map<String, Fragment> mFragments;
    private Table mTable;
    private String mUuid_table;

    private OnFragmentInteractionListener mListener;

    public ConnectFragment() {}

    public static ConnectFragment newInstance(Establishment establishment, ArrayList<String> favorites, String uuid_table) {
        ConnectFragment fragment = new ConnectFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, establishment);
        args.putStringArrayList(ARG_PARAM2, favorites);
        args.putString(ARG_PARAM3, uuid_table);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mEstablishment = getArguments().getParcelable(ARG_PARAM1);
            mFavorites = getArguments().getStringArrayList(ARG_PARAM2);
            mUuid_table = getArguments().getString(ARG_PARAM3);
            mFragments = new HashMap<>();
            mFragments.put(FRAGMENT_DETAIL, DetailEstablishmentFragment.newInstance(mEstablishment, mFavorites));
            mFragments.put(FRAGMENT_OFFERT, OffertFragment.newInstance(mEstablishment.getUuid()));
            mFragments.put(FRAGMENT_STATE, StateFragment.newInstance((mTable == null) ? ((MainActivity)getActivity()).getTable() : mTable, mEstablishment.getUuid(), mUuid_table));
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mListener.setToolbarElevation(0);
    }

    @Override
    public void onResume() {
        super.onResume();
        mListener.setToolbarTitle(mEstablishment.getName());
    }

    @Override
    public void onStop() {
        super.onStop();
        mListener.setToolbarElevation(10);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        mPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());

        for ( String value : key ){
            mPagerAdapter.addFragment(mFragments.get(value), value);
        }

        mViewPager.setAdapter(mPagerAdapter);
        mTabLayout.setupWithViewPager(mViewPager);

        mTabLayout.getTabAt(0).setIcon(R.drawable.attend);
        mTabLayout.getTabAt(1).setIcon(R.drawable.promotion);
        mTabLayout.getTabAt(2).setIcon(R.drawable.detail);

        ((MainActivity)getActivity()).setOnTableChangeListener(this);

        return mView;
    }

    @Override
    public int getFragmentReference() {
        return R.layout.fragment_connect;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onTableChangeListener(Table table) {
        mTable = table;
        Log.i(TAG, table.toString());
        ((StateFragment)mFragments.get(FRAGMENT_STATE)).setTable(table);
    }

    public interface OnFragmentInteractionListener extends ToolbarElevation, ToolbarSetTitle {}
}
