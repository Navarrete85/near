package com.luisignacio.com.near.Models.Database;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Nacho on 14/04/2018.
 */

public class Table implements Parcelable {

    private String id;
    private int state;
    private String user_id;
    private String start_time;
    private String time_of_state_change;
    private String table_id;

    public String getTable_id() {
        return table_id;
    }

    public void setTable_id(String table_id) {
        this.table_id = table_id;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getTime_of_state_change() {
        return time_of_state_change;
    }

    public void setTime_of_state_change(String time_of_state_change) {
        this.time_of_state_change = time_of_state_change;
    }


    public Table() {
    }

    public Table(String id, int state, String user_id) {
        this.id = id;
        this.state = state;
        this.user_id = user_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeInt(this.state);
        dest.writeString(this.user_id);
        dest.writeString(this.start_time);
        dest.writeString(this.time_of_state_change);
        dest.writeString(this.table_id);
    }

    protected Table(Parcel in) {
        this.id = in.readString();
        this.state = in.readInt();
        this.user_id = in.readString();
        this.start_time = in.readString();
        this.time_of_state_change = in.readString();
        this.table_id = in.readString();
    }

    public static final Creator<Table> CREATOR = new Creator<Table>() {
        @Override
        public Table createFromParcel(Parcel source) {
            return new Table(source);
        }

        @Override
        public Table[] newArray(int size) {
            return new Table[size];
        }
    };
}
