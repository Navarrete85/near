package com.luisignacio.com.near.Fragments;


import android.app.AlertDialog;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import com.google.firebase.auth.FirebaseAuth;
import com.luisignacio.com.near.Activitys.AccessActivity;
import com.luisignacio.com.near.BaseModified.BaseFragment;
import com.luisignacio.com.near.R;
import com.luisignacio.com.near.Utils.Utils;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class ForgotPasswdFragment extends BaseFragment {

    private static final String TAG = ForgotPasswdFragment.class.getSimpleName();

    @BindView(R.id.txtForgotPasswd) TextInputLayout mTxtEmail;
    @BindView(R.id.btn_forgot_passwd) Button mSend;

    private ProgressBar mProgressBar;
    private Context context;

    /**
     * Constructor por defecto
     */
    public ForgotPasswdFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);

        return mView;

    }

    @Override
    public int getFragmentReference() {
        return R.layout.fragment_forgot_passwd;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        //Recogemos el contexto de la aplicación como el progressBar
        mProgressBar = ((AccessActivity)context).findViewById(R.id.accessProgressBar);
        this.context = context;

    }

    @OnClick(R.id.btn_forgot_passwd)
    public void forgotAction(View view) {

        //Cogemos el contenido de nuestro editText
        final String email = mTxtEmail.getEditText().getText().toString();

        //Si el mail es de formato válido enviamos password nuevo a correo
        if (Utils.isValidEmail(email)){
            mTxtEmail.setErrorEnabled(false);

            mProgressBar.setVisibility(View.VISIBLE);
            final FirebaseAuth mAuth = FirebaseAuth.getInstance();
            mAuth.sendPasswordResetEmail(email).addOnCompleteListener(task -> {

                int msg, title;
                int icon;

                mProgressBar.setVisibility(View.GONE);
                if ( task.isSuccessful() ){
                    msg = R.string.send_email_passwd;
                    title = R.string.send_ok;
                    icon = R.drawable.ic_markunread_mailbox_black_24dp;
                    mTxtEmail.getEditText().setText("");
                    getFragmentManager().popBackStack();
                }else{
                    msg = R.string.send_fail_msg;
                    title = R.string.send_fail;
                    icon = R.drawable.ic_error_outline_black_24dp;
                    Utils.getAuthError(task.getException(),context, mTxtEmail, null);
                }

                AlertDialog.Builder builder = Utils.getAlertBuilder(title, msg, icon, context);
                builder.show();

            });

        }else{ //De lo contrario notificamos al usuario del error

            mTxtEmail.setError(getResources().getString(R.string.email_error));

        }

    }
}
