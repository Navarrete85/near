package com.luisignacio.com.near.Models.Database;

/**
 * Created by Nacho on 14/04/2018.
 */

public class StateChanges {

    private String[] changes;

    public StateChanges(String[] changes) {
        this.changes = changes;
    }

    public StateChanges() {
    }

    public String[] getChanges() {
        return changes;
    }

    public void setChanges(String[] changes) {
        this.changes = changes;
    }
    
}
