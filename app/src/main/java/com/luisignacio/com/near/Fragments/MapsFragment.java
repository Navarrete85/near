package com.luisignacio.com.near.Fragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;

import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.luisignacio.com.near.Models.Database.Establishment;
import com.luisignacio.com.near.Models.Database.Maps.Marker;
import com.luisignacio.com.near.R;
import com.luisignacio.com.near.Utils.Utils;
import com.luisignacio.com.near.interfaces.ToolbarSetTitle;
import java.util.ArrayList;
import java.util.List;

public class MapsFragment extends Fragment implements OnMapReadyCallback, ClusterManager.OnClusterClickListener<Marker>,
        ClusterManager.OnClusterInfoWindowClickListener<Marker>, ClusterManager.OnClusterItemClickListener<Marker>, ClusterManager.OnClusterItemInfoWindowClickListener<Marker> {

    private static final String TAG = "MapsFragment";
    private boolean start = true;

    private static final String ARG_PARAM1 = "param1";
    private GoogleMap mGoogleMap;
    private MapView mMapView;
    private View mView;
    private Location mLastLocation;
    private LocationRequest mLocationRequest;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private ClusterManager<Marker> mClusterManager;
    private Marker clickedMarkerItem;
    private LatLng last_location;
    private float last_zoom;

    private ArrayList<Establishment> establishments;

    private OnFragmentMapsInteractionListener mListener;

    public MapsFragment() {
    }

    public static MapsFragment newInstance(ArrayList<Establishment> establishments) {

        MapsFragment fragment = new MapsFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_PARAM1, establishments);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.establishments = getArguments().getParcelableArrayList(ARG_PARAM1);
        }

        //Proveedor de localización de google
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_maps, container, false);
        return mView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mMapView = view.findViewById(R.id.mapView);
        if (mMapView != null) {
            mMapView.onCreate(null);
            mMapView.onResume();
            mMapView.getMapAsync(this);
        }

    }

    public void onInfoPressed(Establishment establishment) {
        if (mListener != null) {
            mListener.onEstablishmentSelected(establishment);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentMapsInteractionListener) {
            mListener = (OnFragmentMapsInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsInitializer.initialize(mView.getContext());
        mGoogleMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        mClusterManager = new ClusterManager<Marker>(mView.getContext(), mGoogleMap);
        //Añadimos los marcadores a nuestro mapa
        addMarker();

        mLocationRequest = LocationRequest.create();
        //Intervalo de refresco de ubicación --> 2min
        mLocationRequest.setInterval(120000);
        mLocationRequest.setFastestInterval(120000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);


        mGoogleMap.setOnCameraIdleListener(mClusterManager);
        mGoogleMap.setOnMarkerClickListener(mClusterManager);
        mGoogleMap.setInfoWindowAdapter(mClusterManager.getMarkerManager());
        mGoogleMap.setOnInfoWindowClickListener(mClusterManager);
        mClusterManager.setOnClusterClickListener(this);
        mClusterManager.setOnClusterInfoWindowClickListener(this);
        mClusterManager.setOnClusterItemClickListener(this);
        mClusterManager.setOnClusterItemInfoWindowClickListener(this);

        mClusterManager.getMarkerCollection().setOnInfoWindowAdapter(new MyCustomAdapterForItems());

        //Activamos la localización con el proveedor de google
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }else {
            mFusedLocationProviderClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
            mGoogleMap.setMyLocationEnabled(true);
        }
    }

    private void addMarker() {
        for (Establishment item: establishments){
            mClusterManager.addItem(new Marker(item));
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
        if ( mFusedLocationProviderClient != null ){
            mFusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
        mListener.setToolbarTitle("Mapa");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onStop() {
        super.onStop();
        mMapView.onStop();
    }

    /**
     * Interfaz para la interactuación con la actividad mainActivity
     */
    public interface OnFragmentMapsInteractionListener extends ToolbarSetTitle {
        // TODO: Update argument type and name
        void onEstablishmentSelected(Establishment establishment);
    }

    //Callback encargado de actualizar la posición a partir del servicio de google
    LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            super.onLocationResult(locationResult);
            List<Location> locationList = locationResult.getLocations();
            if (locationList.size() > 0) {
                //The last location in the list is the newest
                Location location = locationList.get(locationList.size() - 1);
                Log.i(TAG, "Location: " + location.getLatitude() + " " + location.getLongitude());
                mLastLocation = location;

                //Place current location marker
                LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                //move map camera
                if ( start ){
                    CameraPosition position = CameraPosition.builder().target(latLng).zoom(14).bearing(0).tilt(45).build();
                    mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(position), 3300, null);
                    start = false;
                    last_location = latLng;
                    last_zoom = mGoogleMap.getCameraPosition().zoom;
                }else{
                    //CameraPosition position = CameraPosition.builder().target(last_location).zoom(15).build();
                    mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(last_location, last_zoom));
                }

            }
        }
    };

    //******************EVENTOS DE LOS MARCADORES Y CLUSTER********************
    @Override
    public boolean onClusterClick(Cluster<Marker> cluster) {

        //Para crear la animación de acercamiento cuando pulsemos un cluster, tenemos que crear un latLang bounds al que le iremos añadiendo cada una
        //de las posiciones de nuestros marcadores...
        LatLngBounds.Builder builder = LatLngBounds.builder();
        for (Marker item : cluster.getItems()) {
            builder.include(item.getPosition());
        }
        // Creamos nuestro latLangBounds a partir de nuestro builder
        final LatLngBounds bounds = builder.build();

        // Animamos la cámara a partir de nuestro latLangBounds
        try {
            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 300));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;

    }

    @Override
    public void onClusterInfoWindowClick(Cluster<Marker> cluster) {}

    @Override
    public boolean onClusterItemClick(Marker marker) {
        clickedMarkerItem = marker;
        return false;
    }

    @Override
    public void onClusterItemInfoWindowClick(Marker marker) {
        onInfoPressed(marker.getEstablishment());
        last_location = marker.getPosition();
        last_zoom = mGoogleMap.getCameraPosition().zoom;
    }

    public class MyCustomAdapterForItems implements GoogleMap.InfoWindowAdapter {

        private final View myContentsView;


        MyCustomAdapterForItems() {
            myContentsView = getActivity().getLayoutInflater().inflate(R.layout.custom_marker, null);
        }

        @Override
        public View getInfoWindow(com.google.android.gms.maps.model.Marker marker) {
            return null;
        }

        @Override
        public View getInfoContents(final com.google.android.gms.maps.model.Marker marker) {
            final TextView tvTitle = ((TextView) myContentsView
                    .findViewById(R.id.tv_title_marker));
            final TextView tvSnippet = ((TextView) myContentsView
                    .findViewById(R.id.tv_description_marker));
            final ImageView occupation = (myContentsView.findViewById(R.id.occupation_level));
            final Establishment establishment = clickedMarkerItem.getEstablishment();
            final ImageView iv_logo = myContentsView.findViewById(R.id.image_marker);

            Glide.with(getContext()).load(establishment.getImage_logo()).apply(new RequestOptions().placeholder(R.drawable.logo).centerCrop()).into(new SimpleTarget<Drawable>() {
                @Override
                public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                    iv_logo.setImageDrawable(resource);

                    if (marker != null && marker.isInfoWindowShown()) {

                        marker.hideInfoWindow();
                        marker.showInfoWindow();

                    }
                }
            });

            if (Utils.isBetween(establishment.getOcupation(), 0f, 50f))
                occupation.setBackgroundColor(getResources().getColor(R.color.low_occupation));
            else if (Utils.isBetween(establishment.getOcupation(), 50f, 75f))
                occupation.setBackgroundColor(getResources().getColor(R.color.medium_occupation));
            else
                occupation.setBackgroundColor(getResources().getColor(R.color.high_occupation));

            tvTitle.setText(establishment.getName());
            final String descripcion = establishment.getDescription();
            tvSnippet.setText((descripcion.length() > 30)?descripcion.substring(0,30) + "..." : descripcion);

            return myContentsView;
        }
    }

}
