package com.luisignacio.com.near.Fragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.luisignacio.com.near.Activitys.MainActivity;
import com.luisignacio.com.near.Adapter.AdapterNearbyEstablishment;
import com.luisignacio.com.near.BaseModified.BaseFragment;
import com.luisignacio.com.near.interfaces.RecyclerViewCLickListener;
import com.luisignacio.com.near.Models.Database.Establishment;
import com.luisignacio.com.near.R;
import com.luisignacio.com.near.interfaces.ToolbarSetTitle;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;


/**
 * Fragmento que va a mostrar un listado de establecimientos cercanos, ordenados por distancia
 */
public class NearFragment extends BaseFragment {

    private static final String TAG = NearFragment.class.getName();
    private static final String ARG = "param1";
    private static final String ARG2 = "param2";
    private static final String ARG3 = "param3";
    private static final String ARG4 = "param4";

    @BindView(R.id.recycler_near) RecyclerView mRecyclerView;

    private ArrayList<Establishment> establishments;
    private OnNearbyFragmentInteractionListener mListener;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private LatLng mLocation;
    private AdapterNearbyEstablishment mAdapter;
    private ArrayList<String> mFavorites;
    private boolean mIs_favorites;
    private ArrayList<Establishment> mEstablishmentFavorites;

    private String title;

    public NearFragment() {}

    /**
     * Creamos la instancia de nuestro fragmento, añadiendole los argumentos necesarios para poder trabajar
     * @param establishments Listado de establecimientos a mostrar.
     * @return A new instance of fragment NearFragment.
     */
    public static NearFragment newInstance(ArrayList<Establishment> establishments, String title, ArrayList<String> favorites, boolean is_favorites) {
        NearFragment fragment = new NearFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG, establishments);
        args.putString(ARG2, title);
        args.putStringArrayList(ARG3, favorites);
        args.putBoolean(ARG4, is_favorites);
        fragment.setArguments(args);

        return fragment;
    }

    /**
     * Recogemos los argumentos que insertamos al crear el fragmento
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            establishments = getArguments().getParcelableArrayList(ARG);
            title = getArguments().getString(ARG2);
            mFavorites = getArguments().getStringArrayList(ARG3);
            mIs_favorites = getArguments().getBoolean(ARG4);
        }

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());
    }

    @Override
    public void onResume() {
        super.onResume();
        mListener.setToolbarTitle(title);
        setFavorites(((MainActivity)getActivity()).getFavorites());
        if ( mIs_favorites ){
            loadFavorites();
            mAdapter.setEstablishmentList(mEstablishmentFavorites);
        }
    }

    public void setFavorites(ArrayList<String> favorites){
        mFavorites = favorites;
        mAdapter.setFavorites(mFavorites);
    }

    /**
     * Creamos la vista que vamos a mostrar
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        if ( mIs_favorites ){
            loadFavorites();
        }

        RecyclerViewCLickListener listener = (view, position) -> {
            if ( mIs_favorites )
                mListener.onEstablishmentSelected(mEstablishmentFavorites.get(position));
            else
                mListener.onEstablishmentSelected(establishments.get(position));
        };
        mAdapter = new AdapterNearbyEstablishment(getContext(),(mIs_favorites) ? mEstablishmentFavorites : establishments, listener, mFavorites);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(mAdapter);
        getLastKnowLocation();

        return mView;
    }

    @Override
    public int getFragmentReference() {
        return R.layout.fragment_near;
    }

    private void loadFavorites() {
        mEstablishmentFavorites = new ArrayList<>();
        if ( mFavorites != null ) {
            for (Establishment item : establishments) {
                if (mFavorites.contains(item.getUuid()))
                    mEstablishmentFavorites.add(item);
            }
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    /**
     * Recogemos el listener que va a estar escuchando en la actividad para que cuando necesitemos interactuar
     * @param context
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnNearbyFragmentInteractionListener) {
            mListener = (OnNearbyFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnNearbyFragmentInteractionListener extends ToolbarSetTitle{
        void onEstablishmentSelected(Establishment establishment);

    }

    private void getLastKnowLocation() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }else{

            mFusedLocationProviderClient.getLastLocation()
                    .addOnSuccessListener(getActivity(), location -> {
                        if (location != null) {
                            mLocation = new LatLng(location.getLatitude(), location.getLongitude());
                            sortedEstablishment();
                            mAdapter.notifyDataSetChanged();
                        }else{
                            getLastKnowLocation();
                        }
                    });

        }

    }

    private void sortedEstablishment(){

        if (establishments.size() == 1)
            establishments.get(0).getDistanceOnPosition(mLocation);

        if (mIs_favorites)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                mEstablishmentFavorites.sort((x, y) -> Float.compare(x.getDistanceOnPosition(mLocation), y.getDistanceOnPosition(mLocation)));
            }else{
                Collections.sort(mEstablishmentFavorites, (x, y) -> Float.compare(x.getDistanceOnPosition(mLocation), y.getDistanceOnPosition(mLocation)));
            }
        else {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                establishments.sort((x, y) -> Float.compare(x.getDistanceOnPosition(mLocation), y.getDistanceOnPosition(mLocation)));
            else{
                Collections.sort(establishments, (x, y) -> Float.compare(x.getDistanceOnPosition(mLocation), y.getDistanceOnPosition(mLocation)));
            }

        }
    }

}
