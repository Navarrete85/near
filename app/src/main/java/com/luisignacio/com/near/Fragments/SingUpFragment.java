package com.luisignacio.com.near.Fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.luisignacio.com.near.Activitys.AccessActivity;
import com.luisignacio.com.near.BaseModified.BaseFragment;
import com.luisignacio.com.near.Models.Database.User;
import com.luisignacio.com.near.R;
import com.luisignacio.com.near.Utils.Utils;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnFocusChange;

import static com.luisignacio.com.near.Utils.Constants.*;
import static com.luisignacio.com.near.Utils.Utils.getFirestoreInstanceWithSetting;

/**
 * Fragmento que se encargará de crear registro de nuevos usuarios
 */
public class SingUpFragment extends BaseFragment implements FirebaseAuth.AuthStateListener, RadioGroup.OnCheckedChangeListener {

    @BindView(R.id.btn_singup) Button mBtnSingUp;
    @BindView(R.id.txtLayoutName) TextInputLayout mTxtName;
    @BindView(R.id.txtLayoutLastName) TextInputLayout mTxtxLastName;
    @BindView(R.id.txtLayoutPasswdSingUp) TextInputLayout mTxtPasswd;
    @BindView(R.id.txtLayoutRepeatPasswdSingUp) TextInputLayout mTxtRepeatPasswd;
    @BindView(R.id.txtLayoutEmailSingUp) TextInputLayout mTxtEmail;
    @BindView(R.id.male) RadioButton mRadioMale;
    @BindView(R.id.female) RadioButton mRadioFemale;
    @BindView(R.id.tvRadioError) TextView mTvRadioError;
    @BindView(R.id.groupGenre) RadioGroup mRadioGroup;

    private boolean checked = false;
    private final String TAG = "SingUpFragment";
    private Context context;
    private ProgressBar mProgressBar;

    private FirebaseAuth mAuth;


    /**
     * Constructor por defecto
     */
    public SingUpFragment() {}


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth = FirebaseAuth.getInstance();
        mAuth.addAuthStateListener(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        mAuth.removeAuthStateListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);
        mRadioGroup.setOnCheckedChangeListener(this);

        return mView;

    }

    @Override
    public int getFragmentReference() {
        return R.layout.fragment_sing_up;
    }

    /**
     * Listener para los eventos click
     * @param view
     */
    @OnClick(R.id.btn_singup)
    public void singUpAction(View view) {

        if ( !checkFields() ){
            AlertDialog.Builder builder = Utils.getAlertBuilder( R.string.error_complete_form_title, R.string.error_complete_form, R.drawable.ic_error_outline_black_24dp, context);
            builder.setPositiveButton(R.string.ok, (dialogInterface, i) -> checkFields()).show();
        }else{
            mProgressBar.setVisibility(View.VISIBLE);
            mTxtRepeatPasswd.setErrorEnabled(false);
            mAuth.createUserWithEmailAndPassword(mTxtEmail.getEditText().getText().toString(), mTxtPasswd.getEditText().getText().toString())
                    .addOnCompleteListener(task -> {

                        if ( task.isSuccessful() ){
                            final FirebaseUser user = mAuth.getCurrentUser();
                            user.sendEmailVerification();
                            saveUser(user.getUid());
                        }else{
                            if(!task.isSuccessful()) {
                                Utils.getAuthError(task.getException(), context, mTxtEmail, mTxtPasswd);
                            }
                            mProgressBar.setVisibility(View.GONE);
                        }
                    });

        }

    }

    /**
     * Listener para los eventos de foco
     * @param view
     * @param hasFocus
     */
    @OnFocusChange({R.id.txtName, R.id.txtLastName, R.id.txtEmail, R.id.txtPasswd, R.id.txtRepeatPasswd})
    public void onFocusChange(View view, boolean hasFocus) {

        if ( !hasFocus ) {

            switch (view.getId()) {

                case R.id.txtName:
                    checkName();
                    break;
                case R.id.txtLastName:
                    checkLastName();
                    break;
                case R.id.txtEmail:
                    checkEmail();
                    break;
                case R.id.txtPasswd:
                    checkPasswd();
                    break;
                case R.id.txtRepeatPasswd:
                    checkRepeatPasswd();
                    break;

            }

        }

    }

    /**
     * Listener para nuestros checkBox
     *
     */
    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {

        if ( !checked ) {
            mTvRadioError.setVisibility(View.GONE);
            checked = true;
        }

    }

    /**
     * Evento de cambio de estado de nuestro estado de autenticación de firebase
     * @param firebaseAuth
     */
    @Override
    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

    }

    /**
     * Cuando se crea el fragmento llama a onAttach, recogemos progressbar y contexto
     * @param context
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        this.context = context;
        mProgressBar = ((AccessActivity)context).findViewById(R.id.accessProgressBar);

    }

    /**
     * Método en el que comprobamos que todos los campos estén cumplimentados correctamente, y en caso de que no lo estén
     * avisamos al usuario del respectivo error en el relleno
     * @return
     */
    private boolean checkFields(){

        int error = 0;

        error += checkName();
        error += checkLastName();
        error += checkEmail();
        error += checkGenre();
        error += checkPasswd();
        error += checkRepeatPasswd();

        if ( error > 0 )
            return false;
        else
            return true;

    }

    private int checkName(){

        if ( mTxtName.getEditText().getText().toString().trim().isEmpty() ) {
            mTxtName.setError(getString(R.string.name_error));
            return 1;
        }else
            mTxtName.setErrorEnabled(false);

        return 0;
    }

    private int checkLastName(){

        if ( mTxtxLastName.getEditText().getText().toString().trim().isEmpty() ) {
            mTxtxLastName.setError(getString(R.string.last_name_error));
            return 1;
        }else
            mTxtxLastName.setErrorEnabled(false);

        return 0;
    }

    private int checkEmail(){

        if ( !Utils.isValidEmail(mTxtEmail.getEditText().getText()) ) {
            mTxtEmail.setError(getString(R.string.email_error));
            return 1;
        }else
            mTxtEmail.setErrorEnabled(false);


        return 0;
    }

    private int checkGenre(){

        if ( !mRadioFemale.isChecked() && !mRadioMale.isChecked() ) {
            mTvRadioError.setVisibility(View.VISIBLE);
            return 1;
        }

        return 0;
    }

    private int checkPasswd(){

        if ( !Utils.isValidPasswd(mTxtPasswd.getEditText().getText()) ) {
            mTxtPasswd.setError(getString(R.string.passwd_error));
            return 1;
        }else
            mTxtPasswd.setErrorEnabled(false);

        return 0;
    }

    private int checkRepeatPasswd(){

        if ( !mTxtPasswd.getEditText().getText().toString().equals(mTxtRepeatPasswd.getEditText().getText().toString()) ) {
            mTxtRepeatPasswd.setError(getString(R.string.passwd_not_equals));
            return 1;
        }else
            mTxtRepeatPasswd.setErrorEnabled(false);

        return 0;
    }

    /**
     * Método con el que guardamos en nuestra base de datos a un usuario que se acaba de registrar
     * @param uuid
     */
    private void saveUser( String uuid ){

        String name = mTxtName.getEditText().getText().toString();
        String lastName = mTxtxLastName.getEditText().getText().toString();
        int genre = mRadioMale.isChecked() ? User.GENDER_MALE : User.GENDER_FEMALE;
        String email = mTxtEmail.getEditText().getText().toString();

        User user = new User(name, lastName, email, genre, uuid);

        getFirestoreInstanceWithSetting().collection(USERS).add(user).addOnCompleteListener(task -> {

            if ( task.isSuccessful() && task.isComplete() ){

               AlertDialog.Builder builder = Utils.getAlertBuilder(R.string.verification_title, R.string.verification_content, R.drawable.ic_markunread_mailbox_black_24dp, context)
                        .setPositiveButton(R.string.ok, (dialogInterface, i) -> clearFields());
                builder.show();

            }else{
               Log.i(TAG, (task.getException() != null) ? task.getException().getMessage() : "Completado correctamente!");
           }

           mProgressBar.setVisibility(View.GONE);

        });

    }

    /**
     * Método con el que limpiamos todos los campos de nuestro formulario
     */
    private void clearFields(){

        mAuth.signOut();
        mTxtxLastName.getEditText().setText("");
        mTxtName.getEditText().setText("");
        mTxtPasswd.getEditText().setText("");
        mTxtRepeatPasswd.getEditText().setText("");
        mTxtEmail.getEditText().setText("");
        mRadioGroup.clearCheck();
        getFragmentManager().popBackStack();

    }

}
