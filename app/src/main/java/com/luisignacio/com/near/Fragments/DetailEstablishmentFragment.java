package com.luisignacio.com.near.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.like.LikeButton;
import com.like.OnLikeListener;
import com.luisignacio.com.near.Activitys.MainActivity;
import com.luisignacio.com.near.BaseModified.BaseFragment;
import com.luisignacio.com.near.Models.Database.Establishment;
import com.luisignacio.com.near.Models.Database.User;
import com.luisignacio.com.near.R;
import com.luisignacio.com.near.interfaces.ToolbarSetTitle;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import java.util.ArrayList;

import butterknife.BindView;

import static com.luisignacio.com.near.Utils.Constants.USERS;
import static com.luisignacio.com.near.Utils.Utils.getFirestoreInstanceWithSetting;

/**
 * Created by Nacho on 05/05/2018.
 */

public class DetailEstablishmentFragment extends BaseFragment implements ImageListener, OnLikeListener {

    private static final String TAG = DetailEstablishmentFragment.class.getSimpleName();
    private static final String FAVORITES = "favorites";
    private static final String ESTABLISHMENT = "establishment";

    @BindView(R.id.carousel_image_establishment_description) CarouselView mCarouselView;
    @BindView(R.id.title_detail_establishment) TextView mDetail_title;
    @BindView(R.id.description_detail_establishment) TextView mDetail_description;
    @BindView(R.id.star_button) LikeButton mLike;

    private Establishment mEstablishment;
    private ToolbarSetTitle mListener;
    private ArrayList<String> mFavorites;

    public static DetailEstablishmentFragment newInstance(Establishment establishment, ArrayList<String> favorites){

        DetailEstablishmentFragment fragment = new DetailEstablishmentFragment();
        Bundle args = new Bundle();
        args.putStringArrayList(FAVORITES, favorites);
        args.putParcelable(ESTABLISHMENT, establishment);
        fragment.setArguments(args);

        return fragment;
    }

    public DetailEstablishmentFragment() {}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        if ( getArguments() != null ){
            mEstablishment = getArguments().getParcelable(ESTABLISHMENT);
            mDetail_title.setText(mEstablishment.getName());
            mDetail_description.setText(mEstablishment.getDescription());
            mFavorites = getArguments().getStringArrayList(FAVORITES);
        }

        if ( mFavorites != null && mFavorites.contains(mEstablishment.getUuid()) )
            mLike.setLiked(true);

        mCarouselView.setPageCount(mEstablishment.getPhotoUrl().size());
        mCarouselView.setImageListener(this);
        mLike.setOnLikeListener(this);

        return mView;
    }

    @Override
    public int getFragmentReference() {
        return R.layout.detail_esblablishment_fragment;
    }

    @Override
    public void setImageForPosition(int position, ImageView imageView) {

        Glide.with(this).load(mEstablishment.getPhotoUrl().get(position)).apply(new RequestOptions().placeholder(R.drawable.prueba).centerCrop()).transition(new DrawableTransitionOptions().crossFade()).into(imageView);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FirstFrameFragment.OnFirstFragmentInteractionListener) {
            mListener = (ToolbarSetTitle) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onStart() {
        super.onStart();
        mListener.setToolbarTitle(mEstablishment.getName());
    }

    @Override
    public void onResume() {
        super.onResume();
        mFavorites = ((MainActivity)getActivity()).getFavorites();
        if ( mFavorites != null && mFavorites.contains(mEstablishment.getUuid()) )
            mLike.setLiked(true);
        else
            mLike.setLiked(false);
    }

    @Override
    public void liked(LikeButton likeButton) {
        likeAction(LikeAction.LIKE);
    }

    @Override
    public void unLiked(LikeButton likeButton) {
        likeAction(LikeAction.UNLIKE);
    }

    private void likeAction(LikeAction action){

        final String uid = mEstablishment.getUuid();
        final DocumentReference userReference = getFirestoreInstanceWithSetting().collection(USERS).document(FirebaseAuth.getInstance().getCurrentUser().getUid());

        userReference.get().addOnSuccessListener( documentSnapshot -> {
            if ( documentSnapshot != null && documentSnapshot.exists() ){
                final User userInfo = documentSnapshot.toObject(User.class);
                final ArrayList<String> likes = (userInfo.getFavorites() == null) ? new ArrayList<>() : userInfo.getFavorites();

                switch (action){
                    case LIKE:
                        if (!likes.contains(uid)) likes.add(uid);
                        break;
                    case UNLIKE:
                        likes.remove(uid);
                        break;
                }

                userInfo.setFavorites(likes);
                userReference.set(userInfo);

            }
        });

    }

    enum LikeAction{
        LIKE,
        UNLIKE
    }

}
