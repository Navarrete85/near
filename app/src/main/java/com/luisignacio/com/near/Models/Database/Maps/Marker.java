package com.luisignacio.com.near.Models.Database.Maps;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;
import com.luisignacio.com.near.Models.Database.Establishment;

/**
 * Created by Nacho on 13/05/2018.
 */

public class Marker implements ClusterItem {

    private final LatLng mPosition;
    private final Establishment establishment;

    public Marker(Establishment establishment) {
        this.establishment = establishment;
        final LatLng position = new LatLng(establishment.getLoc().getLatitude(), establishment.getLoc().getLongitude());
        this.mPosition = position;
    }
    public Establishment getEstablishment() {
        return establishment;
    }

    @Override
    public LatLng getPosition() {
        return mPosition;
    }

}
