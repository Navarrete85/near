package com.luisignacio.com.near.Models.Database;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.firestore.GeoPoint;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Nacho on 14/04/2018.
 */

public class Establishment implements Serializable, Parcelable {

    private String  uuid;
    private ArrayList<Double> l;
    private ArrayList<String> photoUrl;
    private ArrayList<String> offers;
    private float ocupation;
    private String  description;
    private ArrayList<String> workers_id;
    private String name;
    private String image_logo;
    private float distance;
    private String waiter_id;
    private GeoPoint loc;
    private String g;

    public String getG() {
        return g;
    }

    public void setG(String g) {
        this.g = g;
    }

    public GeoPoint getLoc() {
        return loc;
    }

    public void setLoc(GeoPoint loc) {
        this.loc = loc;
    }

    public String getWaiter_id() {
        return waiter_id;
    }

    public float getDistance() {
        return distance;
    }

    public String getImage_logo() {return image_logo;}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Establishment() {
    }
    public String getUuid() {
        return uuid;
    }

    public ArrayList<Double> getL() {
        return l;
    }

    public ArrayList<String> getPhotoUrl() {
        return photoUrl;
    }

    public ArrayList<String> getOffers() {
        return offers;
    }

    public String getDescription() {
        return description;
    }

    public float getOcupation() {return ocupation;}

    public ArrayList<String> getWorkers_id() {return workers_id;}

    public float getDistanceOnPosition(LatLng position){

        float[] result = new float[1];
        Location.distanceBetween(this.loc.getLatitude(), this.loc.getLongitude(), position.latitude, position.longitude, result);
        distance = result[0];

        return distance;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.uuid);
        dest.writeList(this.l);
        dest.writeStringList(this.photoUrl);
        dest.writeStringList(this.offers);
        dest.writeFloat(this.ocupation);
        dest.writeString(this.description);
        dest.writeStringList(this.workers_id);
        dest.writeString(this.name);
        dest.writeString(this.image_logo);
        dest.writeFloat(this.distance);
        dest.writeString(this.waiter_id);
        dest.writeDouble(loc.getLatitude());
        dest.writeDouble(loc.getLongitude());
    }

    protected Establishment(Parcel in) {
        this.uuid = in.readString();
        this.l = new ArrayList<Double>();
        in.readList(this.l, Double.class.getClassLoader());
        this.photoUrl = in.createStringArrayList();
        this.offers = in.createStringArrayList();
        this.ocupation = in.readFloat();
        this.description = in.readString();
        this.workers_id = in.createStringArrayList();
        this.name = in.readString();
        this.image_logo = in.readString();
        this.distance = in.readFloat();
        this.waiter_id = in.readString();
        Double lat = in.readDouble();
        Double lon = in.readDouble();
        this.loc = new GeoPoint(lat, lon);
    }

    public static final Creator<Establishment> CREATOR = new Creator<Establishment>() {
        @Override
        public Establishment createFromParcel(Parcel source) {
            return new Establishment(source);
        }

        @Override
        public Establishment[] newArray(int size) {
            return new Establishment[size];
        }
    };
}
