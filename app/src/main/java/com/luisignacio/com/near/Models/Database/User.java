package com.luisignacio.com.near.Models.Database;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Nacho on 14/04/2018.
 */

public class User implements Serializable {

    public static final int GENDER_MALE = 1,
                            GENDER_FEMALE = 2;

    private String name;
    private String lastName;
    private String email;
    private String address;
    private String photoUrl;
    private ArrayList<String> favorites;
    private String uuid;
    private int gender;
    private String phoneNumber;
    private boolean connect;
    private String establishment_id;
    private String table_id;

    public boolean isConnect() {
        return connect;
    }

    public void setConnect(boolean connect) {
        this.connect = connect;
    }

    public String getEstablishment_id() {
        return establishment_id;
    }

    public void setEstablishment_id(String establishment_id) {
        this.establishment_id = establishment_id;
    }

    public String getTable_id() {
        return table_id;
    }

    public void setTable_id(String table_id) {
        this.table_id = table_id;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public User() {
    }

    public User(String name, String lastName, String email, String address, String photoUrl, ArrayList<String> favorites, String uuid) {
        this.name = name;
        this.lastName = lastName;
        this.email = email;
        this.address = address;
        this.photoUrl = photoUrl;
        this.favorites = favorites;
        this.uuid = uuid;
    }

    public User(String name, String lastName, String email, String address, String uuid) {
        this.name = name;
        this.lastName = lastName;
        this.email = email;
        this.address = address;
        this.uuid = uuid;
    }

    public User(String name, String lastName, String email, int gender, String uuid) {
        this.name = name;
        this.lastName = lastName;
        this.email = email;
        this.gender = gender;
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public ArrayList<String> getFavorites() {
        return favorites;
    }

    public void setFavorites(ArrayList<String> favorites) {
        this.favorites = favorites;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

}




