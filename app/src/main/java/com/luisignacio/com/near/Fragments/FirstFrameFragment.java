package com.luisignacio.com.near.Fragments;

import android.app.AlertDialog;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.accent_systems.ibks_sdk.scanner.ASBleScanner;
import com.accent_systems.ibks_sdk.scanner.ASResultParser;
import com.accent_systems.ibks_sdk.scanner.ASScannerCallback;
import com.accent_systems.ibks_sdk.utils.ASUtils;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.luisignacio.com.near.Activitys.MainActivity;
import com.luisignacio.com.near.BaseModified.BaseFragment;
import com.luisignacio.com.near.Models.Database.Establishment;
import com.luisignacio.com.near.Models.Database.Table;
import com.luisignacio.com.near.Models.Database.User;
import com.luisignacio.com.near.R;
import com.luisignacio.com.near.Utils.Constants;
import com.luisignacio.com.near.Utils.Utils;
import com.luisignacio.com.near.interfaces.DownloadEstablishmentCallback;
import com.luisignacio.com.near.interfaces.ToolbarSetTitle;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
//import de.hdodenhof.circleimageview.CircleImageView;

import static com.luisignacio.com.near.Utils.Constants.ERROR_OCCUPIED_TABLE;
import static com.luisignacio.com.near.Utils.Constants.ERROR_TABLE_NOT_FOUND;
import static com.luisignacio.com.near.Utils.Constants.ESTABLISHMENTS;
import static com.luisignacio.com.near.Utils.Constants.FAVOTIES;
import static com.luisignacio.com.near.Utils.Constants.MAP;
import static com.luisignacio.com.near.Utils.Constants.NEARBY;
import static com.luisignacio.com.near.Utils.Constants.TABLES;
import static com.luisignacio.com.near.Utils.Utils.getFirestoreInstanceWithSetting;


public class FirstFrameFragment extends BaseFragment implements ASScannerCallback, DownloadEstablishmentCallback {

    private static final String TAG = FirstFrameFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";

    @BindView(R.id.textView_conect)
    TextView mTvConect;
    @BindView(R.id.connect)
    View mConnect;
    @BindView(R.id.near)
    View mNear;
    @BindView(R.id.maps)
    View mMaps;
    @BindView(R.id.iv_disabled)
    View mDisabled;
    @BindView(R.id.favorite)
    View mFavorites;
    @BindView(R.id.layout_loader_first_fragment)
    View mLoader;

    private boolean conected;
    private User mUserParam;
    private ASBleScanner mAsBleScanner;

    private List<String> scannedDeivcesList;

    private OnFirstFragmentInteractionListener mListener;

    public FirstFrameFragment() {
    }

    public static FirstFrameFragment newInstance(User user) {

        FirstFrameFragment fragment = new FirstFrameFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, user);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mUserParam.getTable_id() == null) {
            conected = false;
            mTvConect.setText(getResources().getString(R.string.connect));
        } else {
            conected = true;
            mTvConect.setText(getResources().getString(R.string.continue_session));
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mUserParam = (User) getArguments().getSerializable(ARG_PARAM1);
        }

        scannedDeivcesList = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);

        mDisabled.setVisibility((mUserParam.getFavorites() != null && mUserParam.getFavorites().size() > 0) ? View.GONE : View.VISIBLE);

        ((MainActivity)getActivity()).setOnDownloadNearbyCallback(this);

        return mView;
    }

    @Override
    public int getFragmentReference() {
        return R.layout.fragment_first_frame;
    }

    private void starAnimation(final View view, final ArrayList<Establishment> establishments) {

        if (view != null) {
            view.animate().withLayer()
                    .rotationY(720)
                    .setDuration(500)
                    .withEndAction(() -> {
                                view.setRotationY(360);
                                view.animate().withLayer()
                                        .rotationY(0)
                                        .setDuration(500)
                                        .withEndAction(() -> {
                                            mListener.onFirstFragmentInteraction(view.getId(), establishments);
                                            stopLoader();
                                        })
                                        .start();
                            }
                    ).start();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFirstFragmentInteractionListener) {
            mListener = (OnFirstFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onPause() {
        super.onPause();
        stopLoader();
    }

    private void startScan(final boolean showError) {
        int err;
        mAsBleScanner = new ASBleScanner(getActivity(), this);
        mAsBleScanner.setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY);
        err = ASBleScanner.startScan();
        if (err != ASUtils.TASK_OK)
            Log.i(TAG, "startScan - Error (" + Integer.toString(err) + ")");

        new Thread(() -> {
            try {
                Thread.sleep(10000);
                if (!conected) {
                    if (showError) {
                        showError(ERROR_TABLE_NOT_FOUND);
                        ASBleScanner.stopScan();
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();

    }

    @Override
    public void onResume() {
        super.onResume();
        scannedDeivcesList.clear();
        mListener.setToolbarTitle("Inicio");
        mUserParam = ((MainActivity) getActivity()).getUser();
        if ( mUserParam != null )
            mDisabled.setVisibility((mUserParam.getFavorites() != null && mUserParam.getFavorites().size() > 0) ? View.GONE : View.VISIBLE);
    }

    private void showError(int option) {

        if (getActivity() != null) {
            getActivity().runOnUiThread(() -> {

                String title_message = "", content_message = "";

                switch (option) {
                    case ERROR_TABLE_NOT_FOUND:
                        title_message = getResources().getString(R.string.scan_error);
                        content_message = getResources().getString(R.string.scan_error_description);
                        break;
                    case ERROR_OCCUPIED_TABLE:
                        title_message = getResources().getString(R.string.error_occupied_table);
                        content_message = getResources().getString(R.string.error_occupied_table_description);
                        break;
                }

                LayoutInflater factory = LayoutInflater.from(getActivity());
                final View dialogView = factory.inflate(R.layout.disconnect_dialog, null);
                final AlertDialog dialog = new AlertDialog.Builder(getActivity()).create();
                final TextView title = dialogView.findViewById(R.id.title_dialog);
                title.setText(title_message);
                final TextView content = dialogView.findViewById(R.id.content_dialog);
                content.setText(content_message);
                final ImageView image = dialogView.findViewById(R.id.image_dialog);
                image.setImageResource(R.drawable.error_drawable);
                dialog.setView(dialogView);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialogView.findViewById(R.id.btn_dialog).setOnClickListener((view) -> {
                    dialog.dismiss();
                    stopLoader();
                    scannedDeivcesList.clear();
                    return;
                });
                dialog.setCancelable(false);
                dialog.show();
            });
        }
    }

    @Override
    public void scannedBleDevices(ScanResult result) {

        JSONObject advData;
        switch (ASResultParser.getAdvertisingType(result)) {
            case ASUtils.TYPE_EDDYSTONE_UID:
                advData = ASResultParser.getDataFromAdvertising(result);
                if (result.getRssi() > -58) {
                    try {
                        if (!scannedDeivcesList.contains(advData.getString(Constants.ESTABLISHMENT))) {
                            scannedDeivcesList.add(advData.getString(Constants.ESTABLISHMENT));
                            Log.i(TAG, "FrameType = " + advData.getString("FrameType") + " AdvTxPower = " + advData.getString("AdvTxPower") + " Namespace = " + advData.getString("Namespace") + " Instance = " + advData.getString("Instance"));
                            conected = true;
                            isTableFree(advData.getString(Constants.ESTABLISHMENT), advData.getString(Constants.TABLE));

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                break;
        }

    }

    /**
     * Método con el que comprobamos si una mesas que escaneamos está libre para anclar al usuario, o por el contrario
     * se encuentra ocupada.
     *
     * @param establishment_id
     * @param table_id
     */
    public void isTableFree(String establishment_id, String table_id) {

        if (mAsBleScanner != null && ASBleScanner.getmBluetoothAdapter() != null)
            ASBleScanner.stopScan();

        //Recogemos la referencia de nuestra base de datos de la mesa que queremos consultar
        final DocumentReference reference = getFirestoreInstanceWithSetting().getInstance().collection(ESTABLISHMENTS).document(establishment_id).collection(TABLES).document(table_id);
        //Buscamos en nuestra base de datos
        reference.get().addOnCompleteListener((task -> {
            //Si la operación se ha realizado con éxito
            if (task.isSuccessful()) {
                //Recogemos el documento y comprobamos de que existe
                final DocumentSnapshot document = task.getResult();
                if (document.exists()) {
                    //Recogemos la mesa y miramos si está ocupada o estamos ya sentados en ella
                    final Table table = document.toObject(Table.class);
                    if (table.getUser_id() == null || table.getUser_id().equalsIgnoreCase(mUserParam.getUuid())) {
                        //Nos conectamos a la mesa y actualizamos la hora si hace falta
                        goToConnect(establishment_id, table_id, true);
                        if (table.getUser_id() == null || table.getStart_time() == null) {
                            table.setUser_id(mUserParam.getUuid());
                            table.setStart_time(Utils.getStringCurrentTime());
                            reference.set(table);
                        }
                    } else {
                        showError(ERROR_OCCUPIED_TABLE);
                    }
                    return;
                }
            }
            showError(ERROR_TABLE_NOT_FOUND);
        }));

    }

    private void startLoader() {
        mLoader.setVisibility(View.VISIBLE);
        mNear.setEnabled(false);
        mMaps.setEnabled(false);
        mConnect.setEnabled(false);
        mFavorites.setEnabled(false);
    }

    private void stopLoader() {
        mLoader.setVisibility(View.GONE);
        mNear.setEnabled(true);
        mMaps.setEnabled(true);
        mConnect.setEnabled(true);
        mFavorites.setEnabled(true);
    }

    /**
     * @param uuid_establishment
     * @param uuid_table
     * @param saveNewState
     */
    private void goToConnect(final String uuid_establishment, final String uuid_table, final boolean saveNewState) {

        final DocumentReference reference = getFirestoreInstanceWithSetting().getInstance().collection(ESTABLISHMENTS).document(uuid_establishment);
        reference.get().addOnCompleteListener(task -> {
            if (task.isSuccessful() && task.getResult().exists()) {
                if (mAsBleScanner != null && ASBleScanner.getmBluetoothAdapter() != null)
                    ASBleScanner.stopScan();
                Utils.setBluetooth(false);
                conected = true;
                final Establishment establishment = task.getResult().toObject(Establishment.class);
                mUserParam.setEstablishment_id(uuid_establishment);
                mUserParam.setTable_id(uuid_table);
                mConnect.animate().withLayer()
                        .rotationY(720)
                        .setDuration(500)
                        .withEndAction(
                                () -> {

                                    mConnect.setRotationY(360);
                                    mConnect.animate().withLayer()
                                            .rotationY(0)
                                            .setDuration(500)
                                            .withEndAction(() -> {
                                                mListener.onConnectAction(establishment, uuid_table, saveNewState);
                                                stopLoader();
                                            })
                                            .start();

                                }

                        ).start();
            }
        });

    }

    @OnClick({R.id.connect, R.id.near, R.id.maps, R.id.favorite})
    public void onViewClicked(View view) {

        startLoader();

        switch (view.getId()) {
            case R.id.connect:
                if (mUserParam.getTable_id() != null && mUserParam.getEstablishment_id() != null) {
                    goToConnect(mUserParam.getEstablishment_id(), mUserParam.getTable_id(), false);
                } else {
                    if (Utils.isBluetoothActivate())
                        this.startScan(true);
                    else {
                        if (Utils.setBluetooth(true)) {
                            new Thread(() -> {

                                int count = 0;
                                do {
                                    startScan(false);
                                    try {
                                        Thread.sleep(5000);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                    count++;
                                } while (count < 2 && !conected);

                                if (!conected)
                                    startScan(true);

                            }).start();
                        }
                    }
                }

                break;
            case R.id.maps:
                mListener.downloadEstablishmemtFragment(MAP);
                break;
            case R.id.near:
//                downloadEstablishments(view);
                mListener.downloadEstablishmemtFragment(NEARBY);
                break;
            case R.id.favorite:
                if (mUserParam.getFavorites() == null || mUserParam.getFavorites().size() == 0) {
                    Snackbar.make(getView(), getResources().getString(R.string.favorites_is_empty), Toast.LENGTH_SHORT).show();
                    stopLoader();
                } else
                    mListener.downloadEstablishmemtFragment(FAVOTIES);
                break;

        }

    }

    /**
     * Callback para comunicación con Main Activity, nos encargamos de escuchar hasta que se descarguen los establecimientos cercanos
     * @param establishment
     */
    @Override
    public void onDownloadFinished(int view, ArrayList<Establishment> establishment) {
        final View item = (view == NEARBY) ? mNear : (view == MAP) ? mMaps : mFavorites;
        starAnimation(item, establishment);
    }

    /**
     * Interface que nos permitirá la comunicación con main activity
     */
    public interface OnFirstFragmentInteractionListener extends ToolbarSetTitle {
        void onFirstFragmentInteraction(int viewId, ArrayList<Establishment> establishmentHashMap);

        void onConnectAction(Establishment establishment, String uuid_table, boolean saveNewState);

        void downloadEstablishmemtFragment(int view);
    }

}
