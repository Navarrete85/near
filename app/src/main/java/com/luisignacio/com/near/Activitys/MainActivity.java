package com.luisignacio.com.near.Activitys;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;

import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.luisignacio.com.near.BaseModified.BaseMain;
import com.luisignacio.com.near.Fragments.ConnectFragment;
import com.luisignacio.com.near.Fragments.FirstFrameFragment;
import com.luisignacio.com.near.Fragments.NearFragment;
import com.luisignacio.com.near.Models.Database.Establishment;
import com.luisignacio.com.near.Models.Database.User;
import com.luisignacio.com.near.R;
import com.luisignacio.com.near.Utils.Utils;
import org.imperiumlabs.geofirestore.GeoFirestore;

import static com.luisignacio.com.near.Utils.Constants.*;
import static com.luisignacio.com.near.Utils.Utils.getFirestoreInstanceWithSetting;

import java.util.ArrayList;
import java.util.HashMap;
import butterknife.BindView;
//import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Actividad que se encargará de mostrar los diferentes fragmentos con las opciones que puede tomar el usuario
 * (Conectarse, Ver establecimientos cercanos, Mostrar mapa, Mostrar listado de favoritos)
 */
public class MainActivity extends BaseMain implements NavigationView.OnNavigationItemSelectedListener, NearFragment.OnNearbyFragmentInteractionListener, ConnectFragment.OnFragmentInteractionListener {

    private static final String TAG = "MainActivity";

    @BindView(R.id.toolbar) Toolbar mToolbar;
    @BindView(R.id.appBarLayout) AppBarLayout mAppBarLayout;
    @BindView(R.id.progress_bar_main) View mProgressBarMain;
    @BindView(R.id.nav_view) NavigationView mNavigationView;
    @BindView(R.id.tvTitle) TextView mToolbarTitle;

    private Button mBtnCloseSession;
    private ImageView mIvPhoto;
    private TextView mTvUserName, mTvUserEmail;
    //Variable estática que nos mostrará los dialogos que necesitemos
    private static AlertDialog dialog;
    private FirstFrameFragment mFirstFrameFragment;

    private ArrayList<Establishment> list;
    private boolean star;
    private boolean firsFrameFragment;
    static private Context ctx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ctx = this;
        setSupportActionBar(mToolbar);
        mFragments = new HashMap<>();
        list = new ArrayList<>();

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        mAuth = FirebaseAuth.getInstance();
        mAuth.addAuthStateListener(this);

        star = true;
        firsFrameFragment = false;

        //Cargamos el contenido de nuestro menú
        onLoadNavDrawerElement();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        mNavigationView.setNavigationItemSelectedListener(this);
    }

    /**
     * Método que se llama cuando el volvemos a cargar la vista, comprobamos si el usuario está conectado...
     */
    @Override
    protected void onResume() {
        super.onResume();
        checkState();
    }

    /**
     * Método que se llama cuando el botón de atrás es pulsado, lo utilizamos para ocultar nuestro menú en caso de estar desplegado
     */
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }

    }

    /**
     * Cuando se llama a ondestroy quitamos el listener de cambio de estado de firebaseauth
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        mAuth.removeAuthStateListener(this);

        deleteFirestoreListener();

    }

    /**
     * Método que se llama cuando un elemento de nuestro menú es seleccionado
     * @param item
     * @return
     */
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        switch ( item.getItemId() ){

            case R.id.nav_connect:
                break;
            case R.id.nav_map:
                downloadEstablishments(MAP);
                break;
            case R.id.nav_near:
                downloadEstablishments(NEARBY);
                break;
            case R.id.nav_favorite:
                downloadEstablishments(FAVOTIES);
                break;
            case R.id.nav_share:
                break;
            case R.id.btn_close_session:
                closeSession();
                break;

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return false;

    }

    /**
     * Método con el que cogemos los elementos de nuestra vista de menú
     */
    private void onLoadNavDrawerElement(){

        View header = mNavigationView.getHeaderView(0);
        mBtnCloseSession = header.findViewById(R.id.btn_close_session);
        mBtnCloseSession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeSession();
            }
        });

        mIvPhoto = header.findViewById(R.id.image_view_user_photo);
        mTvUserEmail = header.findViewById(R.id.tv_user_email);
        mTvUserName = header.findViewById(R.id.tv_user_name);
        mConexionState = header.findViewById(R.id.conexion_state);

        //Cargamos y ponemos a la escucha al usuario
        loadInformationUser();
    }

    /**
     * Método con el que recogemos los datos de usuario
     */
    private void loadInformationUser() {

        //Referencia a nuestra base de datos, para poder quitar a posteriori el listener
        userFirestoreReference = getFirestoreInstanceWithSetting().collection(USERS).document(mAuth.getCurrentUser().getUid());

        //Listener que escuchará los cambios de estado de nestro usuario
        firestoreUserListener = userFirestoreReference.addSnapshotListener(((documentSnapshot, error) -> {
            if ( error != null ){
                Log.e(TAG, "Error Firestore Listener --> " + error.getMessage());
                return;
            }

            if ( documentSnapshot != null && documentSnapshot.exists() ){
                final User newUser = documentSnapshot.toObject(User.class);
                //Si es la primera vez que entramos, cargamos los datos de usuario, de lo contrario, refrescamos los datos a mostrar
                if ( star ) {
                    user = newUser;
                    loadStart();
                }else
                    refresh(newUser);
            }else{
                Log.e(TAG, "Error...el documento del usuario no existe!");
            }
        }));

    }

    /**
     * Método con el que cargamos los datos de usuario a través de los datos obtenidos en nuestra base de datos
     */
    private void loadStart(){

        mTvUserName.setText((user.getLastName() != null) ? user.getName() + " " + user.getLastName() : user.getName());
        mTvUserEmail.setText(user.getEmail());
        if ( user.getPhotoUrl() != null ){
            Glide.with(this).load(user.getPhotoUrl()).apply(new RequestOptions().placeholder(R.drawable.avatar_male).circleCrop()).into(mIvPhoto);
        }else{

            if ( user.getGender() == User.GENDER_MALE )
                mIvPhoto.setImageResource(R.drawable.avatar_male);
            else
                mIvPhoto.setImageResource(R.drawable.avatar_male);

        }
        if ( user.getTable_id() == null ) {
            Utils.setBluetooth(true);
        }else{
            Utils.setBluetooth(false);
            addTableListener(user.getEstablishment_id(), user.getTable_id());
        }

        checkState();

        mFirstFrameFragment = FirstFrameFragment.newInstance(user);
        changeFragment(mFirstFrameFragment, false);
        star = false;

    }

    /**
     * Método que se llama cuando se modifiquen los datos de nuestro usuario en la base de datos
     * @param newUser
     */
    private void refresh(User newUser){
        Log.i(TAG, "Refresh");
        //Si nos desconectamos
        if ( isDisconnected(newUser) ){

            //Eliminamos el listener de nuestra base de datos
            deleteFirestoreListener();

            //Eliminamos nuestro id en la regerencia a la mesa en la que estamos sentados
            mTable.setUser_id(null);
            tableFirestoreReference.set(mTable).addOnCompleteListener((task -> {
                Log.i(TAG, ((task.isSuccessful())?"Datos salvados en firestore":"Error en el salvado de datos de firestore"));
            }));

            //Creamos el cuadro de dialogo para mostrar el feedback al usuario de la desconexión
            LayoutInflater factory = LayoutInflater.from(getBaseContext());
            final View dialogView = factory.inflate(R.layout.disconnect_dialog, null);
            dialog = new AlertDialog.Builder(ctx).create();
            dialog.setView(dialogView);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            //Acción cuando se pulsa el botón de aceptar
            dialogView.findViewById(R.id.btn_dialog).setOnClickListener((view) ->{
                if ( dialog != null && dialog.isShowing() ) {
                    dialog.dismiss();
                    dialog.cancel();
                    dialog = null;
                }
                new Thread(()->{
                    try {
                        //Nos mandamos directamente a mainActivity, limpiando la pila de Actividades, para que se reinicie el servicio
                        Thread.sleep(300);
                        Intent intent = new Intent(MainActivity.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }).start();
            } );

            dialog.setCancelable(false);
            //Si la actividad no ha terminado...mostramos el cuadro de diálogo
            if (!isFinishing())
                dialog.show();
            return;

        }

        //En caso de que no nos desconectemos....guardamos el usuario con sus cambios y refrescamos los fragmentos
        user = newUser;
        //Vemos si hemos cambiado de estado...por conectarnos
        checkState();
        refreshFragmentInfo();

    }

    /**
     * Método en el que refrescamos el usuario en el caso de que el usuario cambie
     */
    private void refreshFragmentInfo() {

        for ( String key : mFragments.keySet() ){
            ((NearFragment)mFragments.get(key)).setFavorites(user.getFavorites());
        }

    }

    /**
     * Método que se llama cuando el botón de conectar de firstFragment es pulsado
     * @param establishment
     * @param uuid_table
     */
    @Override
    public void onConnectAction( Establishment establishment, String uuid_table, boolean saveNewState ) {

        if (saveNewState) {
            saveNewState();
            addTableListener(establishment.getUuid(), uuid_table);
        }

        changeFragment(ConnectFragment.newInstance(establishment, user.getFavorites(), uuid_table), true);

    }

    /**
     * Método sobreescrito de FirstFrameFragment cuando se pulsa la opción de establecimientos cercanos, lo que
     * vamos a realizar es una consulta a nuestra base de datos para que se nos dé un listado de los establecimientos
     * más cercanos a nosotros(el radio establecido en estos momentos es de 5kn, pudiendo cambiarlo con la constante QUERY_RADIUS)
     */
    @Override
    public void downloadEstablishmemtFragment(int view) {
        firsFrameFragment = true;
        downloadEstablishments(view);
    }

    /**
     * Método con el que cambiamos el nombre de la toolbar, dependiendo de el fragmento que estemos mostrando
     * @param title
     */
    @Override
    public void setToolbarTitle(String title) {
        mToolbarTitle.setText(title);
    }

    /**
     * Listener con el que cambiamos la elevación de la toolbar, nos hace falta por que cuando mostramos el fragmento connect
     * al tener un tablayout, queremos quitar la elevación para que se vea de forma homogenea junto al toolbar
     * @param elevation
     */
    @Override
    public void setToolbarElevation(float elevation) {
        mAppBarLayout.setElevation(elevation);
    }

    /**
     * Método con el que descargamos una lista de establecimientos de nuestra base de datos
     * @param option --> Para saber que vista vamos a cargar y si necesitamos todos los establecimientos o por el contrario sólamente los favoritos
     */
    private void downloadEstablishments( int option ) {
        if (!firsFrameFragment)
            startLoader();

        final CollectionReference reference = getFirestoreInstanceWithSetting().collection(ESTABLISHMENTS);
        list.clear();
        geoFirestore = new GeoFirestore(reference);

        if ( option == NEARBY ){
            getLastKnowLocation();
        }else {

            reference.get().addOnCompleteListener(task -> {

                if (task.isSuccessful()) {

                    for (QueryDocumentSnapshot document : task.getResult()) {
                        final Establishment establishment = document.toObject(Establishment.class);
                        list.add(establishment);
                        //Con esto observamos si un establecimiento tiene el hash para buscar por cercanía, en caso de que no lo
                        //tenga, agregamos el hash para la posibilidad de búsqueda...esto lo hacemos en el listado de favoritos
                        //puesto que aquí descargamos todos los establecimientos que disponemos en nuestra base de datos
                        if ( document.get("g") == null || document.get("l") == null )
                            geoFirestore.setLocation(establishment.getUuid(), establishment.getLoc());
                    }

                    if ( firsFrameFragment ) {
                        if (nearbyCallback != null) {
                            nearbyCallback.onDownloadFinished(option, list);
                            firsFrameFragment = false;
                        }
                    }else
                        goToEstablishmentList(option, list);

                }

            });

        }

    }

    private void goToEstablishmentList( int option, ArrayList<Establishment> establishments ){

        switch (option){
            case MAP:
            case NEARBY:
                onFirstFragmentInteraction((option == MAP) ? R.id.maps : R.id.near, establishments);
                break;
            case FAVOTIES:
                ArrayList<Establishment> favorites = new ArrayList<>();
                if ( user.getFavorites() != null ) {
                    for (Establishment item : establishments) {
                        if (user.getFavorites().contains(item.getUuid()))
                            favorites.add(item);
                    }
                }
                onFirstFragmentInteraction(R.id.favorite, favorites);
        }

        stopLoader();

    }

    /**
     * Lanzamos el loader
     */
    private void startLoader(){
        mProgressBarMain.setVisibility(View.VISIBLE);
    }

    /**
     * Escondemos el loader
     */
    private void stopLoader(){
        mProgressBarMain.setVisibility(View.GONE);
    }

    // TODO: 17/08/2018 **********************Implementación listener GeoFire***********************
    /**
     * Método que se llama cada vez que encontramos un documento que esté en el radio de establecimientos
     * establecido por nuestro filtro
     * @param documentSnapshot
     * @param geoPoint
     */
    @Override
    public void onDocumentEntered(DocumentSnapshot documentSnapshot, GeoPoint geoPoint) {
        if ( documentSnapshot.exists() ){
            list.add(documentSnapshot.toObject(Establishment.class));
        }
    }

    @Override
    public void onDocumentExited(DocumentSnapshot documentSnapshot) {}

    @Override
    public void onDocumentMoved(DocumentSnapshot documentSnapshot, GeoPoint geoPoint) {}

    @Override
    public void onDocumentChanged(DocumentSnapshot documentSnapshot, GeoPoint geoPoint) {}

    /**
     * Eliminamos el listener una vez se ha completado la obtención de datos (se ha llamado todas las veces necesarias
     * a onDocumentEntered)
     */
    @Override
    public void onGeoQueryReady() {
        geoQuery.removeGeoQueryEventListener(this);
        if ( firsFrameFragment ) {
            if (nearbyCallback != null) {
                nearbyCallback.onDownloadFinished(NEARBY, list);
                firsFrameFragment = false;
            }
        }else
            goToEstablishmentList(NEARBY, list);
    }

    @Override
    public void onGeoQueryError(Exception e) {}
}
