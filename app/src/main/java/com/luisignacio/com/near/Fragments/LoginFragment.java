package com.luisignacio.com.near.Fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentActivity;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.TwitterAuthProvider;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.luisignacio.com.near.Activitys.AccessActivity;
import com.luisignacio.com.near.Activitys.MainActivity;
import com.luisignacio.com.near.BaseModified.BaseFragment;
import com.luisignacio.com.near.Models.Database.User;
import com.luisignacio.com.near.R;
import com.luisignacio.com.near.Utils.Constants;
import com.luisignacio.com.near.Utils.Utils;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.SessionManager;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import java.io.ByteArrayOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.OnClick;

import static com.luisignacio.com.near.Utils.Utils.getFirestoreInstanceWithSetting;


public class LoginFragment extends BaseFragment implements FirebaseAuth.AuthStateListener{

    public static final String TAG = LoginFragment.class.getSimpleName();

    private OnLoginFragmentListener mListener;
    @BindView(R.id.txtLayoutEmail) TextInputLayout mTxtEmail;
    @BindView(R.id.txtLayoutPasswd) TextInputLayout mTxtPasswd;
    @BindView(R.id.iv_facebook) ImageView  mIvFacebook;
    @BindView(R.id.iv_google) ImageView  mBtnGoogle;
    @BindView(R.id.btn_facebook) LoginButton mBtnFacebook;
    @BindView(R.id.btn_login_twitter) TwitterLoginButton mTwitterLoginButton;

    private Context context;
    private ProgressBar mProgressBar;
    private CallbackManager mCallbackManager;
    private GoogleApiClient mGoogleApiClient;

    private boolean twitter;

    private FirebaseAuth mAuth;
    private FirebaseUser mFirebaseUser;

    /**
     * Constructor por defecto, requerido para instanciar el fragmento
     */
    public LoginFragment() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAuth = FirebaseAuth.getInstance();

//        initializeGooglePlus();

    }

    /**
     * En onCreateView devolvemos la vista del fragmento
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);

        Log.i(TAG, LoginManager.getInstance().toString());

        showPackageInfo();

        initializeTwitter();
        initializeFacebook();

        return mView;
    }

    @Override
    public int getFragmentReference() {
        return R.layout.fragment_login;
    }

    private void singUpToFirebaseWithTwitter(TwitterSession data) {

        AuthCredential credential = TwitterAuthProvider.getCredential(data.getAuthToken().token, data.getAuthToken().secret);

        mAuth.signInWithCredential(credential).addOnCompleteListener(getActivity(), task -> {

            if ( task.isSuccessful() ){
                Log.i(TAG, "Autenticacion con Twitter correcta");
            }else{
                Log.i(TAG, "Autenticacion con Twitter erronea");
            }

        });

    }

    /**
     * Manejador para cuando nos autenticamos con facebook
     * @param accessToken
     */
    private void handleFacebookAccessToken(AccessToken accessToken) {

        final AuthCredential credential = FacebookAuthProvider.getCredential(accessToken.getToken());
        mAuth.signInWithCredential(credential).addOnCompleteListener(getActivity(), task -> {

            if ( task.isSuccessful()) {
                Log.i(TAG, "Autenticacion con Facebook correcta");
                Log.i(TAG, "*********************" + task.getResult().getUser().getEmail());
            }
            else
                Log.i(TAG, "Autenticacion con Facebook erronea");
        });

    }

    @Override
    public void onStart() {
        super.onStart();

        mFirebaseUser = mAuth.getCurrentUser();
        mAuth.addAuthStateListener(this);

        showPackageInfo();

    }

    @Override
    public void onStop() {
        super.onStop();
        mAuth.removeAuthStateListener(this);

    }

    @Override
    public void onResume() {
        super.onResume();

        Log.i(TAG, "Entro en onresume");

        mTxtPasswd.setError(null);
        mTxtEmail.setError(null);
        mTxtEmail.setErrorEnabled(false);
        mTxtPasswd.setErrorEnabled(false);

    }

    @OnClick({R.id.iv_google, R.id.iv_facebook, R.id.tv_twitter, R.id.btn_login, R.id.btn_singup, R.id.txt_get_new})
    public void buttonAction(View view) {

        switch (view.getId()){

            case R.id.btn_singup:
            case R.id.txt_get_new:
                onButtonPressed(view.getId());
                break;
            case R.id.btn_login:
                twitter = false;
                checkLogin();
                break;
            case R.id.iv_facebook:
                mBtnFacebook.performClick();
                twitter = true;
                break;
            case R.id.tv_twitter:
                mTwitterLoginButton.performClick();
                twitter = true;
                break;
            case R.id.iv_google:
                loginWithGoogle();
                break;

        }

    }

    /**
     * Método con el que comprobamos el login que realiza el usuario
     */
    private void checkLogin() {

        final CharSequence email = mTxtEmail.getEditText().getText(),
                           passwd = mTxtPasswd.getEditText().getText();

        if ( validate( email, passwd ) ){

            if ( mFirebaseUser == null ){
                mProgressBar.setVisibility(View.VISIBLE);
                mAuth.signInWithEmailAndPassword(email.toString().trim(), passwd.toString().trim())
                        .addOnCompleteListener(getActivity(), task -> {

                            mProgressBar.setVisibility(View.GONE);
                            if ( task.isSuccessful() ){}
                            else{
                                //Se necesita poner error en el ingreso de passwd o email
                                Toast.makeText(getActivity(), "Error autenticación aqui " + task, Toast.LENGTH_SHORT).show();
                            }

                        });

            }else{
                mAuth.signOut();
            }

        }

    }

    /**
     * Método con el que nos pasamos a la actividad main, sin crear cola para que cuando demos a back no nos lleve a la actividad de
     * registro.
     */
    private void goToMain() {

        Intent intent = new Intent(getActivity(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        mProgressBar.setVisibility(View.GONE);
        startActivity(intent);

    }

    /**
     * Método con el que el contenido introducido en los campos de email y passwd son correctos, en caso de que
     * algún campo no siga con el patrón establecido mostramos el error en el respectivo campo de insercción de datos
     * @param email
     * @param passwd
     * @return
     */
    private boolean validate( CharSequence email, CharSequence passwd ){

        mTxtPasswd.setError(null);
        mTxtEmail.setError(null);
        mTxtEmail.setErrorEnabled(false);
        mTxtPasswd.setErrorEnabled(false);

        if ( !Utils.isValidEmail(email) ){

            mTxtEmail.setErrorEnabled(true);

            if ( email.toString().trim().isEmpty() )
                mTxtEmail.setError(getResources().getString(R.string.empty_email));
            else
                mTxtEmail.setError(getResources().getString(R.string.email_error));

            return false;

        }

        if ( !Utils.isValidPasswd(passwd) ){

            mTxtPasswd.setErrorEnabled(true);

            if ( passwd.toString().trim().isEmpty() )
                mTxtPasswd.setError(getResources().getString(R.string.empty_passwd));
            else
                mTxtPasswd.setError(getResources().getString(R.string.passwd_error));

            return false;

        }

        return true;

    }

    /**
     * Evento que nos comunica con la actividad a través de la interfaz creada para este proposito
     * @param id
     */
    public void onButtonPressed( int id ) {
        if (mListener != null) {
            mListener.onLoginFragmentInteraction(id);
        }
    }

    /**
     * Se lanza este evento cuando la actividad se asocia con el Fragment, recogemos el contexto y con el damos instancia a nuestro
     * listener para poder tener una comunicación con nuestra actividad principal
     * @param context
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        this.context = context;
        this.mGoogleApiClient = ((AccessActivity)getActivity()).getmGoogleApiClient();

        mProgressBar = ((AccessActivity) context).findViewById(R.id.accessProgressBar);

        if (context instanceof OnLoginFragmentListener) {
            mListener = (OnLoginFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnLoginFragmentListener");
        }
    }

    /**
     * Se llama cuando el Fragmento muere, por eso tenemos que pasar a null a nuestro listener de comunicación
     */
    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        mAuth.removeAuthStateListener(this);
    }

    /**
     * Evento que se llama cuando el estado de autenticación firebase cambia de estado
     * @param firebaseAuth
     */
    @Override
    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

        Log.i(TAG, (firebaseAuth.getCurrentUser() != null && firebaseAuth.getCurrentUser().isEmailVerified())?"Email validado":"Email no válido");

        Log.i("Change", "State change " + ((firebaseAuth.getCurrentUser() != null) ? firebaseAuth.getCurrentUser().getEmail(): "Null"));
        final FirebaseUser user = firebaseAuth.getCurrentUser();
        if ( user != null && (user.isEmailVerified() || twitter) ){

            Log.i(TAG, "Autenticación correcta");
            checkIfUserExist(user);

        }else if ( user != null && !user.isEmailVerified() ){

                AlertDialog.Builder builder = Utils.getAlertBuilder(R.string.verified_error_title, R.string.verified_error, R.drawable.ic_mail_16, context);

                builder.setPositiveButton(getResources().getString(R.string.ok), (dialogInterface, i) -> {});
                builder.show();
                mAuth.signOut();

        }else{
            Log.i(TAG, "Error de autenticación ");
        }

    }

    /**
     * Interfaz de comunicación con AccessActivity
     */
    public interface OnLoginFragmentListener {
        // TODO: Update argument type and name
        void onLoginFragmentInteraction(int id);
    }

    private void loginWithGoogle(){

        mProgressBar.setVisibility(View.VISIBLE);
        Intent intent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(intent, Constants.REQUEST_GOOGLE_SING_IN);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        mCallbackManager.onActivityResult(requestCode, resultCode, data);
        mTwitterLoginButton.onActivityResult(requestCode, resultCode, data);

        Log.i(TAG, (mTwitterLoginButton == null )?"null":"no null");
        Log.i(TAG, "CallbackFacebook" + ((mCallbackManager == null )?"null":"no null"));
        Log.i(TAG, "btnGoogle" + ((mBtnGoogle == null )?"null":"no null"));
        Log.i(TAG, "ivFacebook" + ((mIvFacebook == null )?"null":"no null"));

        Log.i(TAG, "Onactivityresult ");
        switch (requestCode){

            case Constants.REQUEST_GOOGLE_SING_IN:
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                handleResult(result);
                break;
             default:
                 Log.i(TAG, "Onactivityresult facebook");

        }

    }

    /**
     * Manejador del resultado cuando nos autenticamos con google
     * @param result
     */
    private void handleResult(GoogleSignInResult result) {

        if ( result.isSuccess() ){

            //Recogemos nuestro objeto que contendrá la información del usuario
            final GoogleSignInAccount account = result.getSignInAccount();
            final AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
            mAuth.signInWithCredential(credential).addOnCompleteListener(getActivity(), task -> {

                if ( !task.isSuccessful() ){

                    Toast.makeText(context, "No se ha podido autenticar con Google", Toast.LENGTH_SHORT).show();

                }else{

                    Log.i(TAG, "Login con google correcto!");
                    Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(status -> {

                            if (status.isSuccess())
                                Log.i(TAG, "Sesion google cerrada");
                            else
                                Log.i(TAG, "La sesión de google no se ha podido cerrar");
                        });

                }

            });

        }else{

            Toast.makeText(context, "No se pudo autenticar", Toast.LENGTH_SHORT).show();
            Log.i(TAG, result.toString() + "\n" + result.getStatus().toString());

        }

        mProgressBar.setVisibility(View.GONE);

    }

    /**
     * Método con el que comprobamos si el usuario que hemos registrado se encuentra en nuestra base de datos, en caso de que no
     * lo tengamos almacenado, llamamos al método que se encarga de guardar en nuestra base de datos
     * @param user
     */
    private void checkIfUserExist(final FirebaseUser user) {

        mProgressBar.setVisibility(View.VISIBLE);

        final FirebaseFirestore db = getFirestoreInstanceWithSetting();

        db.collection("users").document(user.getUid()).get().addOnCompleteListener((task -> {
            if (task.isSuccessful()){
                final DocumentSnapshot document = task.getResult();
                if ( !document.exists() ){
                    addUserForDatabase(user);
                }else {
                    mProgressBar.setVisibility(View.GONE);
                    goToMain();
                }
            }else{
                Log.e(TAG, "Error en la consulta checkIfUserExist firestore");
            }
        }));

    }

    /**
     * Método encargado de guardar un usuario en nuestra base de datos
     * @param user
     */
    private void addUserForDatabase( FirebaseUser user ) {

        final User newUser = new User();
        //Si tenemos a disposición photo url de nuestro usuario la recogemos para guardarla
        newUser.setPhotoUrl((user.getPhotoUrl() != null ) ? user.getPhotoUrl().toString() : null);
        newUser.setEmail(user.getEmail());
        newUser.setUuid(user.getUid());
        newUser.setPhoneNumber(user.getPhoneNumber());
        newUser.setName(user.getDisplayName());

        mProgressBar.setVisibility(View.VISIBLE);

        if ( newUser.getPhotoUrl() != null )
            saveImage(newUser);
        else
            saveFirestoreUser(newUser);

    }

    /**
     * Método con el que guardamos la imagen del usuario autenticado en storage de firebase, una vez guardada
     * salvamos en database nuestro usuario apuntando a la url de la imagen que tenemos almacenada en storage
     * @param newUser
     */
    private void saveImage(final User newUser){

        Log.i(TAG, newUser.getPhotoUrl());

        Glide.with(this).load(newUser.getPhotoUrl()).into(new SimpleTarget<Drawable>() {
            @Override
            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                final StorageReference storageReference = FirebaseStorage.getInstance().getReference().child("userImages/" + newUser.getUuid() + ".png");
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                Bitmap bitmap = Utils.drawableToBitmap(resource);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] data = baos.toByteArray();
                UploadTask uploadTask = storageReference.putBytes(data);
                uploadTask.addOnSuccessListener(taskSnapshot -> {

                    User user = newUser;
                    //user.setPhotoUrl(taskSnapshot.getMetadata().getReference().getDownloadUrl().toString());
                    //Con la nueva actualización de storage no se nos devuelve la referencia a nuestra url de descarga, por lo que tenermos que hacer una nueva
                    //petición a nuestro documento alojado en la nube y obtener la url de descarga
                    taskSnapshot.getStorage().getDownloadUrl().addOnSuccessListener(uriTask -> {
                        user.setPhotoUrl(uriTask.toString());
                        saveFirestoreUser(user);
                    });

                }).addOnFailureListener(e -> saveFirestoreUser(newUser));
            }
        });

    }

    /**
     * Método con el que guardamos un usuario en nuestra base de datos
     * @param user
     */
    private void saveFirestoreUser( User user ){

        final FirebaseFirestore db = getFirestoreInstanceWithSetting();

        db.collection("users").document(user.getUuid()).set(user).addOnCompleteListener((task -> {
            Log.i(TAG, ((task.isSuccessful())?"Datos salvados en firestore":"Error en el salvado de datos de firestore"));
            goToMain();
        }));

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        SessionManager<TwitterSession> sessionManager = TwitterCore.getInstance().getSessionManager();

        if ( sessionManager.getActiveSession() != null )
            sessionManager.clearActiveSession();

        if ( LoginManager.getInstance() != null )
            LoginManager.getInstance().logOut();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.i(TAG, "Entro en onDestroyView");
    }

    private void initializeFacebook(){

        mCallbackManager = CallbackManager.Factory.create();

        mBtnFacebook.setReadPermissions(Arrays.asList(
                "email"));

        mBtnFacebook.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                handleFacebookAccessToken(loginResult.getAccessToken());
                Log.i(TAG, "OK inicio de sesión con facebook");
            }

            @Override
            public void onCancel() {
                Snackbar.make(getView(), "Ha cancelado el inicio de sesión con facebook", BaseTransientBottomBar.LENGTH_SHORT).show();
                Log.i(TAG, "Ha cancelado el inicio de sesión con facebook");
            }

            @Override
            public void onError(FacebookException error) {
                Log.i(TAG, error.getMessage());
            }
        });

    }

    private void initializeTwitter(){

        if ( LoginManager.getInstance() != null )
            LoginManager.getInstance().logOut();
        TwitterAuthConfig authConfig = new TwitterAuthConfig(getResources().getString(R.string.consumer_key_twitter),
                getResources().getString(R.string.consumer_secret_twitter));
        TwitterConfig twitterConfig = new TwitterConfig.Builder(context)
                .twitterAuthConfig(authConfig)
                .build();

        Twitter.initialize(twitterConfig);

        mTwitterLoginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                Log.i(TAG, "Entro en success twitter");
                singUpToFirebaseWithTwitter(result.data);
            }

            @Override
            public void failure(TwitterException exception) {
                Log.i(TAG, "Entro en failure twitter");
            }
        });

    }


    private void showPackageInfo(){

        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(
                    "com.luisignacio.com.near",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.i("KeyHash", "Error " + e.getMessage());

        } catch (NoSuchAlgorithmException e) {
            Log.i("KeyHash", "Error " + e.getMessage());
        }

    }

}

/**
 *      ********************NOTA************************
 + Podemos activar el uso de múltiples cuentas con una misma dirección de email, lo que pasa es que cuando utilizamos esta modalidad
 + Los proveedores no nos facilitan el email del usuario, si los demás datos como nombre y url de imagen de perfil..
 + En estos momentos tenemos activada esta opción.
 */