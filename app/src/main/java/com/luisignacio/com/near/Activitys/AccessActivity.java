package com.luisignacio.com.near.Activitys;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.VideoView;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.luisignacio.com.near.BaseModified.BaseActivity;
import com.luisignacio.com.near.Fragments.ForgotPasswdFragment;
import com.luisignacio.com.near.Fragments.LoginFragment;
import com.luisignacio.com.near.Fragments.SingUpFragment;
import com.luisignacio.com.near.R;

import butterknife.BindView;

/**
 * Actividad encargada del acceso de usuario (login, creación de usuario, recordar contraseña)
 */
public class AccessActivity extends BaseActivity implements LoginFragment.OnLoginFragmentListener, GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = "AccessActivity";

    @BindView(R.id.videoView) VideoView mVideoView;
    @BindView(R.id.logo) ImageView mLogo;
    @BindView(R.id.accessProgressBar) ProgressBar mProgressBar;

    private LoginFragment mLoginFragment;
    private GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initializeGooglePlus();
        Uri path = Uri.parse("android.resource://com.luisignacio.com.near/" + R.raw.background_video2);

        mVideoView.setVideoURI(path);
        mVideoView.setOnPreparedListener(mediaPlayer -> {
            mediaPlayer.setLooping(true);
            mVideoView.start();
        });

        //Establecemos el fragmento principal
        mLoginFragment = new LoginFragment();
        changeFragment(mLoginFragment, false);

    }

    /**
     * Método abstracto que tenemos que definir para ahorrar el bind de butterknife, puesto que la actividad hereda
     * de nuestra clase BaseActivity
     * @return
     */
    @Override
    public int getViewId() {
        return R.layout.activity_access;
    }

    /**
     * Cuando se llama a onStart, comenzamos la animación del logo (jugando con la capa alfa)
     */
    @Override
    protected void onStart() {
        super.onStart();

        Animation animation = new AlphaAnimation(1, 0.5f);
        animation.setDuration(6000);
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(Animation.INFINITE);
        animation.setRepeatMode(Animation.REVERSE);

        mLogo.startAnimation(animation);

    }

    /**
     * Método que se llama cuando queremos cambiar de fragmento a presentar
     * @param fragment --> Fragmento a presentar
     * @param backStack --> Boolean que indica si queremos mantener el fragmento a reemplazar en la pila
     */
    private void changeFragment(Fragment fragment, boolean backStack ){

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        ft.setCustomAnimations(R.animator.fragment_slide_left_enter,
                R.animator.fragment_slide_left_exit,
                R.animator.fragment_slide_right_enter,
                R.animator.fragment_slide_right_exit);

        if ( !backStack ) {
            ft.replace(R.id.contentLayout, fragment).commit();
        }else {
            ft.replace(R.id.contentLayout, fragment).addToBackStack("tag").commit();
        }

    }

    /**
     * Método que se llama cuando obtenemos resultados de otra actividad, en este caso lo usamos para la
     * autenticación con facebook, twitter y google+, tenémos que llamar al onActivityResult del fragmento que se encarga
     * del login para que trate el resultado y ver si la autenticación es correcta
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if ( mLoginFragment != null )
            mLoginFragment.onActivityResult(requestCode, resultCode, data);

    }

    /**
     * Interfaz que hemos implementado de nuestro Fragmento login, para que cuando se pulse el botón de
     * crear cuenta o recordar cambiemos el fragmento a mostrar
     * @param id
     */
    @Override
    public void onLoginFragmentInteraction( int id ) {

        switch (id){

            case R.id.txt_get_new:
               changeFragment(new ForgotPasswdFragment(), true);
                break;
            case R.id.btn_singup:
                changeFragment(new SingUpFragment(), true);
                break;

        }

    }

    private void initializeGooglePlus(){

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getResources().getString(R.string.default_web_client_id))
                .requestEmail()
                .requestId()
                .requestProfile()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this,1, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();



    }

    public GoogleApiClient getmGoogleApiClient() {
        return mGoogleApiClient;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.stopAutoManage(this);
            mGoogleApiClient.disconnect();
        }
    }
}
