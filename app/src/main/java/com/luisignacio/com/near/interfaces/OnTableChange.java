package com.luisignacio.com.near.interfaces;

import com.luisignacio.com.near.Models.Database.Table;

/**
 * Created by Nacho on 28/05/2018.
 */

public interface OnTableChange {
    void onTableChangeListener(Table table);
}
