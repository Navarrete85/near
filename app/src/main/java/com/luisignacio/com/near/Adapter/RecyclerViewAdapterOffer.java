package com.luisignacio.com.near.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.luisignacio.com.near.Models.Database.Offers;
import com.luisignacio.com.near.R;
import com.luisignacio.com.near.interfaces.RecyclerViewCLickListener;
import com.squareup.picasso.Picasso;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Nacho on 05/05/2018.
 */

public class RecyclerViewAdapterOffer extends RecyclerView.Adapter<RecyclerViewAdapterOffer.MyViewHolder> {

    private Context ctx;
    private List<Offers> mOfferts;
    private View mParent;
    private RecyclerViewCLickListener mListener;

    public RecyclerViewAdapterOffer(Context ctx, List<Offers> mOfferts, View parent, RecyclerViewCLickListener listener) {
        this.ctx = ctx;
        this.mOfferts = mOfferts;
        mParent = parent;
        mListener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        view = LayoutInflater.from(ctx).inflate(R.layout.offer_item, parent, false);
        final MyViewHolder viewHolder = new MyViewHolder(view, mListener);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.binOffer(mOfferts.get(position));
    }

    @Override
    public int getItemCount() {
        return mOfferts.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.title_card_offer) TextView mOffer_title;
        @BindView(R.id.description_card_offer) TextView mOffer_description;
        @BindView(R.id.image_card_offer) ImageView mOffer_image;
        @BindView(R.id.btn_attention_call_card_offer) Button mOffer_btn_attention;
        @BindView(R.id.disabled_card_offer) View mDisabled_view;

        private RecyclerViewCLickListener mListener;

        public MyViewHolder(View itemView, RecyclerViewCLickListener listener) {

            super(itemView);

            ButterKnife.bind(this, itemView);
            mListener = listener;

        }

        public void binOffer( Offers offer ){

            mOffer_title.setText((offer.getTitle() != null) ? offer.getTitle() : "");
            mOffer_description.setText((offer.getDescription() != null) ? offer.getDescription().replace("\\n", "\n") : "");
            if ( offer.getPhotoUrl() != null ){
                Glide.with(mOffer_image.getContext()).load(offer.getPhotoUrl()).apply(new RequestOptions().placeholder(R.drawable.prueba).centerCrop()).transition(DrawableTransitionOptions.withCrossFade()).into(mOffer_image);
            }else{
                mOffer_image.setImageResource(R.drawable.prueba);
            }

            if ( !offer.isEnabled() ){
                mDisabled_view.setVisibility(View.VISIBLE);
                mOffer_btn_attention.setEnabled(false);
            }

        }

        @OnClick(R.id.btn_attention_call_card_offer)
        public void onRequestAttention( View view){mListener.onClick(view, getAdapterPosition());}
    }

}
