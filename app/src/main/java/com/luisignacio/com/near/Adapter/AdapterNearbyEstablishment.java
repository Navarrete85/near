package com.luisignacio.com.near.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.like.LikeButton;
import com.luisignacio.com.near.Models.Database.Establishment;
import com.luisignacio.com.near.R;
import com.luisignacio.com.near.Utils.Utils;
import com.luisignacio.com.near.interfaces.RecyclerViewCLickListener;

import java.text.DecimalFormat;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Nacho on 19/05/2018.
 */

public class AdapterNearbyEstablishment extends RecyclerView.Adapter<AdapterNearbyEstablishment.NearbyHolder>{

    private Context ctx;
    private ArrayList<Establishment> establishmentList;
    private RecyclerViewCLickListener mListener;
    private ArrayList<String> mFavorites;

    /**
     * Constructor parametrizado
     * @param ctx contexto de la actividad
     * @param establishmentList listado de establecimientos
     * @param mListener listener para escuchar el evento click
     */
    public AdapterNearbyEstablishment(Context ctx, ArrayList<Establishment> establishmentList, RecyclerViewCLickListener mListener, ArrayList<String> favorites) {
        this.ctx = ctx;
        this.establishmentList = establishmentList;
        this.mListener = mListener;
        this.mFavorites = favorites;
    }

    @Override
    public NearbyHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(ctx).inflate(R.layout.nearby_establishment_item, parent, false);
        final NearbyHolder viewHolder = new NearbyHolder(view, mListener);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(NearbyHolder holder, int position) {
        holder.onBinItem(establishmentList.get(position));
    }

    @Override
    public int getItemCount() {
        return establishmentList.size();
    }

    /**
     * Setter para actualizar la lista de favoritos, en caso de que haya algún cambio en esta...
     * @param favorites
     */
    public void setFavorites(ArrayList<String> favorites){
        this.mFavorites = favorites;
        notifyDataSetChanged();
    }

    public void setEstablishmentList(ArrayList<Establishment> establishmentList){
        this.establishmentList = establishmentList;
        notifyDataSetChanged();
    }

    public class NearbyHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.image_card_nearby) ImageView mIvEstablishment;
        @BindView(R.id.nearby_color_ocuppation) ImageView mOcuppationColor;
        @BindView(R.id.tv_card_nearby_name) TextView mTitle;
        @BindView(R.id.tv_card_nearby_description) TextView mDescription;
        @BindView(R.id.tv_card_nearby_occupation) TextView mOccupation;
        @BindView(R.id.tv_card_nearby_distance) TextView mDistance;
        @BindView(R.id.card_nearby) View mView;
        @BindView(R.id.star_button) LikeButton mLikeButton;

        public NearbyHolder(View itemView, RecyclerViewCLickListener listener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mListener = listener;
        }

        public void onBinItem( Establishment establishment ){

            Glide.with(ctx).load(establishment.getPhotoUrl().get(0)).apply(new RequestOptions().placeholder(R.drawable.prueba).centerCrop()).transition(new DrawableTransitionOptions().crossFade()).into(mIvEstablishment);
            mTitle.setText(establishment.getName());
            mDescription.setText(establishment.getDescription() + "...");
            mOccupation.setText(String.valueOf(establishment.getOcupation()) + "%");
            mDistance.setText(getDistance(establishment.getDistance()));
            if (Utils.isBetween(establishment.getOcupation(), 0f, 50f))
                mOcuppationColor.setBackgroundColor(ctx.getResources().getColor(R.color.low_occupation));
            else if (Utils.isBetween(establishment.getOcupation(), 50f, 75f))
                mOcuppationColor.setBackgroundColor(ctx.getResources().getColor(R.color.medium_occupation));
            else
                mOcuppationColor.setBackgroundColor(ctx.getResources().getColor(R.color.high_occupation));

            if ( mFavorites != null && mFavorites.contains(establishment.getUuid()))
                mLikeButton.setLiked(true);
            else
                mLikeButton.setLiked(false);

        }

        @OnClick(R.id.card_nearby)
        public void onSelectElement(View view){
            mListener.onClick(view, getAdapterPosition());
        }

        public String getDistance(float distance){

            final DecimalFormat df = new DecimalFormat();
            df.setMaximumFractionDigits(2);

            if ( distance < 1000 ){
                df.setMaximumFractionDigits(0);
                return String.valueOf(df.format(distance)) + "m";
            }else{
                return String.valueOf(df.format(distance/1000)) + "Km";
            }

        }

    }

}
