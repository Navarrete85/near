package com.luisignacio.com.near.interfaces;

/**
 * Created by Nacho on 25/05/2018.
 */

public interface ToolbarSetTitle {
    void setToolbarTitle(String title);
}
