package com.luisignacio.com.near.Fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.luisignacio.com.near.Adapter.RecyclerViewAdapterOffer;
import com.luisignacio.com.near.BaseModified.BaseFragment;
import com.luisignacio.com.near.interfaces.RecyclerViewCLickListener;
import com.luisignacio.com.near.Models.Database.Offers;
import com.luisignacio.com.near.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

import static com.luisignacio.com.near.Utils.Utils.getFirestoreInstanceWithSetting;

/**
 * Created by Nacho on 05/05/2018.
 */

public class OffertFragment extends BaseFragment {

    private static final String TAG = OffertFragment.class.getSimpleName();

    @BindView(R.id.recycler_offert) RecyclerView mRecyclerView;

    private Context ctx;
    private List<Offers> listOfferts;
    private RecyclerViewAdapterOffer mAdapter;

    private static final String ARG_PARAM1 = "param1";

    private String establishment_id;


    public OffertFragment() {
    }

    public static OffertFragment newInstance( String establishment_id ){
        OffertFragment fragment = new OffertFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, establishment_id);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onAttach(Context context) {

        ctx = context;
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);

        //Listener que nos manada el elemento seleccionado
        RecyclerViewCLickListener listener = (view, position) -> {
            Snackbar.make(getView(), "Elemento seleccionado " + listOfferts.get(position).getTitle(), Toast.LENGTH_SHORT).show();
        };
        mAdapter = new RecyclerViewAdapterOffer(getContext(), listOfferts, mView, listener);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(mAdapter);

        return mView;
    }

    @Override
    public int getFragmentReference() {
        return R.layout.offer_fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if ( getArguments() != null ){
            establishment_id = getArguments().getString(ARG_PARAM1);
            Log.i(TAG, "ID del establecimiento --> " + establishment_id);
        }
        listOfferts = new ArrayList<>();
        loadOffers();

    }

    private void loadOffers() {

        final FirebaseFirestore db = getFirestoreInstanceWithSetting();

        db.collection("establishment").document(establishment_id).collection("offers").get().addOnCompleteListener((task -> {
            if (task.isSuccessful()){
                for (QueryDocumentSnapshot document : task.getResult() ){
                    final Offers item = document.toObject(Offers.class);
                    if ( item.isEnabled() )
                        listOfferts.add(0, item);
                    else
                        listOfferts.add(item);
                }
                mAdapter.notifyDataSetChanged();
            }else{
                Log.e(TAG, "Error al realizar la tarea de obtención de ofertas!!!");
            }
        }));

    }

}
