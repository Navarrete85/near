package com.luisignacio.com.near.Utils;

import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.luisignacio.com.near.R;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

//import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Nacho on 08/04/2018.
 */

public class Utils {

    private static final String TAG = "Utils";

    public final static boolean isValidEmail(final CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static boolean isValidPasswd(final CharSequence passwd) {

        final String PASSWORD_PATTERN = "((?=.*[a-z])(?=.*[A-Z]).{6,20})";
        Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
        return pattern.matcher(passwd).matches();

    }

    public static AlertDialog.Builder getAlertBuilder(int title, int msg, int icon, Context context){

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setMessage(msg)
                .setTitle(title)
                .setIcon(icon);

        return builder;
    }

    public static void getAuthError(Exception exception, Context context, TextInputLayout mTxtEmail, TextInputLayout mTxtPasswd){

        try {
            throw exception;
        } catch(FirebaseAuthWeakPasswordException e) {
            mTxtPasswd.setError(context.getString(R.string.passwd_error));
            mTxtPasswd.requestFocus();
        } catch(FirebaseAuthInvalidCredentialsException e) {
            mTxtEmail.setError(context.getString(R.string.email_error));
            mTxtEmail.requestFocus();
        } catch(FirebaseAuthUserCollisionException e) {
            mTxtEmail.setError(context.getString(R.string.error_user_exists));
            mTxtEmail.requestFocus();
        }catch (FirebaseAuthInvalidUserException e){
            mTxtEmail.setError(context.getString(R.string.invalid_email));
            mTxtEmail.requestFocus();
        }
        catch(Exception e) {
            Log.e(TAG, e.toString());
        }

    }

    public static Bitmap getBitmapFromURL(String src) {

        try {
            java.net.URL url = new java.net.URL(src);
            HttpURLConnection connection = (HttpURLConnection) url
                    .openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    public static void onLoadNavigationDrawer(Context context, ImageView imageUser, TextView username, TextView userEmail){

        final NavigationView navigationView = ((AppCompatActivity)context).findViewById(R.id.nav_view);
        final View header = navigationView.getHeaderView(0);
        imageUser = header.findViewById(R.id.image_view_user_photo);
        username = header.findViewById(R.id.tv_user_name);
        userEmail = header.findViewById(R.id.tv_user_email);


    }

    public static boolean setBluetooth(boolean enable) {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        boolean isEnabled = bluetoothAdapter.isEnabled();
        if (enable && !isEnabled) {
            return bluetoothAdapter.enable();
        }
        else if(!enable && isEnabled) {
            return bluetoothAdapter.disable();
        }

        return true;
    }

    public static boolean isBluetoothActivate(){

        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if ( bluetoothAdapter != null && bluetoothAdapter.isEnabled() )
            return true;
        else
            return false;

    }

    public static boolean isBetween(float number, float lower, float upper) {
        return lower <= number && number <= upper;
    }

    public static String getStringCurrentTime(){
        final DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        final Date date = new Date();
        return dateFormat.format(date).toString();
    }

    public static Bitmap drawableToBitmap (Drawable drawable) {

        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable)drawable).getBitmap();
        }

        int width = drawable.getIntrinsicWidth();
        width = width > 0 ? width : 1;
        int height = drawable.getIntrinsicHeight();
        height = height > 0 ? height : 1;

        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;

    }

    public static FirebaseFirestore getFirestoreInstanceWithSetting(){

        final FirebaseFirestore firestore = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(false)
                .setTimestampsInSnapshotsEnabled(true)
                .build();
        firestore.setFirestoreSettings(settings);

        return firestore;

    }

}

